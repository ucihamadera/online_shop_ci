<?php
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP
 *
 * This content is released under the MIT License (MIT)
 *
 * Copyright (c) 2014 - 2015, British Columbia Institute of Technology
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @package	CodeIgniter
 * @author	EllisLab Dev Team
 * @copyright	Copyright (c) 2008 - 2014, EllisLab, Inc. (http://ellislab.com/)
 * @copyright	Copyright (c) 2014 - 2015, British Columbia Institute of Technology (http://bcit.ca/)
 * @license	http://opensource.org/licenses/MIT	MIT License
 * @link	http://codeigniter.com
 * @since	Version 1.0.0
 * @filesource
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['upload_userfile_not_set'] = 'Tidak dapat menemukan variabel post bernama \'userfile\'.';
$lang['upload_file_exceeds_limit'] = 'Ukuran file yang diupload melebihi ukuran yang diperbolehkan pada Konfigurasi file PHP.';
$lang['upload_file_exceeds_form_limit'] = 'Ukuran file yang diupload melebihi ukuran yang diperbolehkan pada form.';
$lang['upload_file_partial'] = 'File hanya diupload sebagian.';
$lang['upload_no_temp_directory'] = 'Foler Temporary hilang.';
$lang['upload_unable_to_write_file'] = 'File tidak dapat dimasukan ke lokasi penyimpanan.';
$lang['upload_stopped_by_extension'] = 'Proses upload file dihentikan oleh eksntensi.';
$lang['upload_no_file_selected'] = 'Anda belum memilih file untuk diupload.';
$lang['upload_invalid_filetype'] = 'Jenis file yang anda upload tidak diizinkan.';
$lang['upload_invalid_filesize'] = 'Ukuran file yang anda upload terlalu besar.';
$lang['upload_invalid_dimensions'] = 'Dimensi gambar yang anda upload tidak benar.';
$lang['upload_destination_error'] = 'Tidak dapat memindahkan file ke lokasi yang dituju.';
$lang['upload_no_filepath'] = 'Lokasi upload tidak valid.';
$lang['upload_no_file_types'] = 'Anda belum menentukan tipe file yang diizinkan.';
$lang['upload_bad_filename'] = 'Nama file yang anda masukan sudah terdapat di server.';
$lang['upload_not_writable'] = 'Folder yang ditujukan untuk upload tidak dapat dibaca/diakses.';
