<section id="cart_items">
	<div class="container">
		<div class="breadcrumbs">
			<ol class="breadcrumb">
				<li><a href="<?php echo base_url(); ?>">Home</a></li>
				<li><a href="<?php echo base_url('user'); ?>">Member</a></li>
				<li class="active">Input Pembayaran</li>
			</ol>
		</div>
		<div class="panel panel-default">
			<div class="panel-heading"><h3><i class="fa fa-bookmark"></i> Masukan Detail Pembayaran</h3></div>
			<div class="panel-body text-warning">
				<?php echo $msg = $this->session->flashdata('resultkonf')?'<div class="alert alert-danger text-center">'.$this->session->flashdata('resultkonf').'</div>':''; ?>
				<?php echo $error =  validation_errors() ? '<div class="alert alert-warning">'.validation_errors().'</div>':''; ?>
				<?php echo form_open('user/kirimPembayaran'); ?>
				<legend>Data Pembayaran</legend>
				<input type="hidden" name="kode" value="<?php echo $kode; ?>">
				<div class="form-group">
					<label for="">Tanggal Bayar</label>
					<input required type="text" class="form-control datepicker" name="tgl_bayar" placeholder="Masukan Tanggal Bayar">
				</div>
				<div class="form-group">
					<label for="">Jumlah Bayar</label>
					<input required type="text" class="form-control" name="jml_bayar" placeholder="Masukan Jumlah Transfer">
				</div>
				<div class="form-group">
					<label for="">No. Rekening Bank</label>
					<input required type="text" class="form-control" name="no_rek" placeholder="Masukan No Rekening">
				</div>
				<div class="form-group">
					<label for="">Atas Nama</label>
					<input required type="text" class="form-control" name="atas_nama" placeholder="Atas Nama">
				</div>
				<div class="form-group">
					<label for="">Bank Tujuan</label>
					<select required class="form-control" name="bank_tujuan">
						<option value="">Pilih Bank</option>
						<?php foreach ($bank->result() as $value) { ?>
						<option value="<?php echo $value->nama_bank; ?>"><?php echo $value->nama_bank.' #No.Rekening : '.$value->no_rekening.' #Atas Nama : '.$value->atas_nama; ?></option>
						<?php } ?>
					</select>
				</div>
				<button type="submit" class="btn btn-primary">Kirim Konfirmasi</button>
				<?php echo form_close(); ?>
			</div>
			<div class="panel-footer"></div>
		</div>
	</section>
