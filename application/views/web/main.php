<div class="home-page">
	<div class="col-sm-9 padding-right">
		<div class="features_items"><!--features_items-->
			<h2 class="title text-center">Produk Terbaru</h2>
			<?php if($produk->num_rows() == 0) { ?>
			<div class="panel panel-default">
				<div class="panel-body text-center">
					Belum Ada Produk
				</div>
			</div>
			<?php } ?>
			<?php 
			foreach ($produk->result_array() as $key => $value) { 
				$d = array ('-','/','\\',',','.','#',':',';','\'','"','[',']','{','}',')','(','|','`','~','!','@','%','$','^','&','*','=','?','+');
				$link = strtolower(str_replace($d,"",$value['nama_produk']));
				?>
				<div class="col-sm-4">
					<div class="product-image-wrapper">
						<div class="single-products">
							<div class="productinfo text-center">
								<img alt="<?php echo  $value['nama_produk']; ?>" height="210" src="<?php echo base_url(); ?>asset/images/produk/<?php echo $value['gambar']; ?>" alt="" />
								<h2><?php echo 'Rp '.number_format($value['harga'], 2); ?></h2>
								<p><?php echo  $value['nama_produk']; ?></p>
								<?php echo form_open('keranjang/tambah_barang'); ?>
								<input type="hidden" name="id" value="<?php echo $value['kode_produk']; ?>">
								<input type="hidden" name="price" value="<?php echo $value['harga']; ?>">
								<input type="hidden" name="name" value="<?php echo $value['nama_produk']; ?>">
								<input type="hidden" name="pic" value="<?php echo $value['gambar']; ?>">
								<input type="hidden" name="berat" value="<?php echo $value['berat']; ?>">
								<input type="hidden" name="qty" value="1">
								<button type="submit" class="btn btn-default add-to-cart">
									<i class="fa fa-shopping-cart"></i>
									Beli
								</button>
								<?php echo form_close(); ?>
							</div>
							<div class="product-overlay">
								<div class="overlay-content">
									<h2><?php echo 'Rp '.number_format($value['harga'], 2); ?></h2>
									<p><?php echo $value['nama_produk']; ?></p>
									<?php echo form_open('keranjang/tambah_barang'); ?>
									<input type="hidden" name="id" value="<?php echo $value['kode_produk']; ?>">
									<input type="hidden" name="price" value="<?php echo $value['harga']; ?>">
									<input type="hidden" name="name" value="<?php echo $value['nama_produk']; ?>">
									<input type="hidden" name="pic" value="<?php echo $value['gambar']; ?>">
									<input type="hidden" name="berat" value="<?php echo $value['berat']; ?>">
									<input type="hidden" name="qty" value="1">
									<button type="submit" class="btn btn-default add-to-cart">
										<i class="fa fa-shopping-cart"></i>
										Beli
									</button>
									<?php echo form_close(); ?>
								</div>
							</div>
						</div>
						<div class="choose">
							<ul class="nav nav-pills nav-justified">
								<li><a href="<?php echo base_url('produk/detail').'/'.$value['kode_produk'].'/'.str_replace(' ', '-', strtolower($link)).'.html'; ?>"><i class="fa fa-plus-square"></i>Lihat Detail</a></li>
								<li><a href="<?php echo base_url('produk'); ?>"><i class="fa fa-plus-square"></i>Semua Produk</a></li>
							</ul>
						</div>
					</div>
				</div>
				<?php } ?>
			</div><!--features_items-->
			<div class="recommended_items"><!--recommended_items-->
				<h2 class="title text-center">Artikel Terbaru</h2>
				<?php if($art_active->num_rows() == 0) { ?>
				<div class="panel panel-default">
					<div class="panel-body text-center">
						Belum Ada Artikel
					</div>
				</div>
				<?php } ?>
				<div id="recommended-item-carousel" class="carousel slide" data-ride="carousel">
					<div class="carousel-inner">
						<div class="item active">
							<?php foreach ($art_active->result_array() as $berita) {
								$d = array ('-','/','\\',',','.','#',':',';','\'','"','[',']','{','}',')','(','|','`','~','!','@','%','$','^','&','*','=','?','+');
								$judul = strtolower(str_replace($d,"",$berita['judul']));
								?>
								<div class="col-sm-4">
									<div class="product-image-wrapper">
										<div class="single-products">
											<div class="productinfo text-center">
												<img height="150" alt="<?php echo $berita['judul']; ?>" src="<?php echo base_url(); ?>asset/images/artikel/<?php echo $berita['gambar']; ?>" alt="" />
												<h5><?php echo $berita['judul']; ?></h5>
												<a href="<?php echo base_url('artikel/detail').'/'.$berita['kode_artikel'].'/'.str_replace('-', '/', $berita['tanggal']).'/'.str_replace(' ', '-', strtolower($judul)).'.html'; ?>" class="btn btn-default add-to-cart">Teruskan Baca</a>
											</div>
										</div>
									</div>
								</div>
								<?php } ?>
							</div>
							<div class="item">
								<?php foreach ($art_item->result_array() as $berita) { 
									$d = array ('-','/','\\',',','.','#',':',';','\'','"','[',']','{','}',')','(','|','`','~','!','@','%','$','^','&','*','=','?','+');
									$judul = strtolower(str_replace($d,"",$berita['judul']));
									?>
									<div class="col-sm-4">
										<div class="product-image-wrapper">
											<div class="single-products">
												<div class="productinfo text-center">
													<img height="150" alt="<?php echo $berita['judul']; ?>" src="<?php echo base_url(); ?>asset/images/artikel/<?php echo $berita['gambar']; ?>" alt="" />
													<h5><?php echo $berita['judul']; ?></h5>
													<a href="<?php echo base_url('artikel/detail').'/'.$berita['kode_artikel'].'/'.str_replace('-', '/', $berita['tanggal']) .'/'.str_replace(' ', '-', strtolower($judul)).'.html'; ?>" class="btn btn-default add-to-cart">Teruskan Baca</a>
												</div>
											</div>
										</div>
									</div>
									<?php } ?>
								</div>
							</div>
							<a class="left recommended-item-control" href="#recommended-item-carousel" data-slide="prev">
								<i class="fa fa-angle-left"></i>
							</a>
							<a class="right recommended-item-control" href="#recommended-item-carousel" data-slide="next">
								<i class="fa fa-angle-right"></i>
							</a>			
						</div>
					</div><!--/recommended_items-->
				</div>
</div> <!-- home-page -->