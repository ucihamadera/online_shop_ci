<div class="col-sm-9">
	<h2 class="title text-center">Testimoni</h2>
	<div class="response-area">
		<h2><?php echo count($testimoni->result()); ?> TESTIMONI</h2>
		<ul class="media-list">
			<?php $no=1; foreach ($testimoni->result() as $value) { 
				$waktu = explode('_', $value->waktu);
				?>
				<li class="media <?php echo $st = ($no % 2 == 0) ? 'second-media':'';  ?>">
					<div class="media-body">
						<ul class="sinlge-post-meta">
							<li><i class="fa fa-user"></i><?php echo $value->nama; ?></li>
							<li><i class="fa fa-clock-o"></i> <?php echo $waktu[1]; ?></li>
							<li><i class="fa fa-calendar"></i> <?php echo $waktu[0]; ?></li>
						</ul>
						<p><?php echo $value->isi; ?></p>
						<a class="btn btn-primary" href=""><i class="fa fa-user"></i> Aktif</a>
					</div>
				</li>
				<?php $no++; } ?>
			</ul>					
		</div><!--/Response-area-->
		<div class="replay-box">
			<div class="testi_msg"></div>
			<div class="row">
				<div class="col-sm-4">
					<h2>Kirim Testimoni</h2>
					<?php echo form_open('testimoni/simpan_testimoni', array('onsubmit'=>'return false', 'id'=>'form_testi')); ?>
					<div class="blank-arrow">
						<label>Nama</label>
					</div>
					<span>*</span>
					<input required placeholder="Masukan Nama Anda..." type="text" name="nama">
					<div class="blank-arrow">
						<label>Email</label>
					</div>
					<span>*</span>
					<input required placeholder="Masukan email anda..." name="email" type="email">
				</div>
				<div class="col-sm-8">
					<div class="text-area">
						<div class="blank-arrow">
							<label>Testimoni</label>
						</div>
						<span>*</span>
						<textarea required name="isi" rows="11"></textarea>
						<button type="submit" id="btn_testi" class="btn btn-primary">Kirim Testimoni</button>
						<?php echo form_close(); ?>
					</div>
				</div>
			</div>
		</div><!--/Repaly Box-->
	</div>
	<script type="text/javascript">
	$("#form_testi").submit(function() {
						// menampung data
						var base = '<?php echo base_url(); ?>';
						data = $("#form_testi").serialize();
						$("#btn_testi").html('Memproses...');
						$.ajax({
							url: $(this).prop('action'),
							type: 'POST',
							dataType: 'json',
							data: data,
							success: function(msg) {
								if(msg.success==true) { //jika login berhasil maka muncul pesan sukses
									$('.testi_msg').removeClass('alert alert-warning text-center').addClass('alert alert-info text-center').html(msg.isi);
									$(':input').val('');
								}
								else
								{
									//jika login gagal maka muncul pesan error
									$('.testi_msg').addClass('alert alert-warning text-center').html(msg.isi);
								}
							},
							complete:function(){
								$("#btn_testi").html('Kirim Testimoni <i class="fa fa-sign-in"></i>');
							}
						});
					});
	</script>