<section id="cart_items">
	<div class="container">
		<div class="breadcrumbs">
			<ol class="breadcrumb">
				<li><a href="<?php echo base_url(); ?>">Home</a></li>
				<li><a href="<?php echo base_url('user'); ?>">Member</a></li>
				<li class="active">Pengaturan Profil</li>
			</ol>
		</div>
		<div class="panel panel-default">
			<div class="panel-heading"></div>
			<div class="panel-body">
				<?php echo $msg = $this->session->flashdata('result')?'<div class="alert alert-warning">'.$this->session->flashdata('result').'</div>':''; ?>
				<?php echo $error =  validation_errors() ? '<div class="alert alert-warning">'.validation_errors().'</div>':''; ?>
				<div class="alert alert-warning text-center">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<strong>Info:</strong> Anda akan logout otomatis jika proses ubah password berhasil.
				</div>
				<?php echo form_open('user/ubah_profil', array('role'=>'form')); ?>
				<legend>Pengaturan Profil</legend>
				<input type="hidden" name="kode" value="<?php echo $this->session->userdata('kode'); ?>">
				<div class="form-group">
					<label for="">Email</label>
					<input readonly type="text" name="email" class="form-control" id="" value="<?php echo $this->session->userdata('email'); ?>">
				</div>
				<div class="form-group">
					<label for="">Password Lama</label>
					<input required type="text" name="pass_lama" class="form-control" id="" placeholder="Masukan Password Lama">
				</div>
				<div class="form-group">
					<label for="">Password Baru</label>
					<input required type="text" name="pass_baru" class="form-control" id="" placeholder="Masukan Password Baru">
				</div>
				<div class="form-group">
					<label for="">Ulangi Password Baru</label>
					<input required type="text" name="ulang_pass" class="form-control" id="" placeholder="Ulangi Password Baru">
				</div>
				<button type="submit" class="btn btn-primary">Ubah Password</button>
				<?php echo form_close(); ?>
			</div>
			<div class="panel-footer"></div>
		</div>
	</section>