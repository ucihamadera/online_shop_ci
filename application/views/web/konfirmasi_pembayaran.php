<section id="cart_items">
	<div class="container">
		<div class="breadcrumbs">
			<ol class="breadcrumb">
				<li><a href="<?php echo base_url(); ?>">Home</a></li>
				<li><a href="<?php echo base_url('user'); ?>">Member</a></li>
				<li class="active">Konfirmasi Pembayaran</li>
			</ol>
		</div>
		<div class="panel panel-default">
			<div class="panel-heading"><h3><i class="fa fa-bookmark"></i> Konfirmasi Pembayaran</h3></div>
			<div class="panel-body text-danger">
				<?php if($konfirmasi->num_rows() == 0 ) {echo 'Belum Ada Transaksi';} else { ?>
				<?php echo $msg = $this->session->flashdata('result')?'<div class="alert alert-warning">'.$this->session->flashdata('result').'</div>':''; ?>
				<div class="alert alert-warning">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<strong>Note : </strong> Anda masih bisa mengubah alamat pengiriman selama anda belum melakukan konfirmasi pembayaran.
				</div>
				<table class="table table-hover">
					<thead>
						<tr>
							<th>No</th>
							<th>Tanggal Pesan</th>
							<th>Total Bayar</th>
							<th>Status</th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						<?php $no=1; foreach ($konfirmasi->result() as $value) { ?>
						<tr>
							<td><?php echo $no; ?></td>
							<td><?php echo $this->Adminmodel->ubahTanggal($value->tgl_order); ?></td>
							<td><?php echo 'Rp '. number_format($value->total_bayar, 2); ?></td>
							<td><?php echo $value->status; ?></td>
							<td>
								<a class="btn btn-danger" href="<?php echo base_url('user/input_pembayaran').'/'.$value->kode_order; ?>"><i class="fa fa-share"></i> Konfirmasi</a>
								<a class="btn btn-danger" href="<?php echo base_url('user/detail_pembayaran').'/'.$value->kode_order; ?>"><i class="fa fa-star"></i> Detail Transaksi</a>
								<a class="btn btn-danger" href="<?php echo base_url('user/ubah_alamat').'/'.$value->kode_order; ?>"><i class="fa fa-pencil"></i> Ubah Alamat</a>
							</td>
						</tr>
						<?php $no++; } ?>
					</tbody>
				</table>
				<?php } ?>
			</div>
			<div class="panel-footer"></div>
		</div>
	</section>
