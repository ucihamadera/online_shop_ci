<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="<?php echo $nama .'|'. $deskripsi; ?>">
	<meta name="author" content="Emade Haryo Kuncoro">
	<title>Home | E-Shopper</title>
	<link href="<?php echo base_url(); ?>asset/css/bootstrap.min.css" rel="stylesheet">
	<link href="<?php echo base_url(); ?>asset/css/font-awesome.min.css" rel="stylesheet">
	<link href="<?php echo base_url(); ?>asset/css/prettyPhoto.css" rel="stylesheet">
	<link href="<?php echo base_url(); ?>asset/css/animate.css" rel="stylesheet">
	<link href="<?php echo base_url(); ?>asset/css/main.css" rel="stylesheet">
	<link href="<?php echo base_url(); ?>asset/css/responsive.css" rel="stylesheet">
	<script src="<?php echo base_url(); ?>asset/js/jquery.js"></script>
	<script src="<?php echo base_url(); ?>asset/js/bootstrap.min.js"></script>
	<script src="<?php echo base_url(); ?>asset/js/jquery.scrollUp.min.js"></script>
	<script src="<?php echo base_url(); ?>asset/js/jquery.prettyPhoto.js"></script>
	<script src="<?php echo base_url(); ?>asset/js/main.js"></script>
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="<?php echo base_url(); ?>asset/images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url(); ?>asset/images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url(); ?>asset/images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url(); ?>asset/images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="<?php echo base_url(); ?>asset/images/ico/apple-touch-icon-57-precomposed.png">
</head><!--/head-->
<body>
	<header id="header"><!--header-->
		<div class="header_top"><!--header_top-->
			<div class="container">
				<div class="row">
					<div class="col-sm-6">
						<div class="contactinfo">
							<ul class="nav nav-pills">
								<li><a href="#"><i class="fa fa-phone"></i> <?php echo $no_telp; ?></a></li>
								<li><a href="#"><i class="fa fa-envelope"></i> <?php echo $email; ?></a></li>
							</ul>
						</div>
					</div>
					<div class="col-sm-6">
						<div class="social-icons pull-right">
							<ul class="nav navbar-nav">
								<li><a href="<?php echo $fb; ?>"><i class="fa fa-facebook"></i></a></li>
								<li><a href="<?php echo $twitter; ?>"><i class="fa fa-twitter"></i></a></li>
								<li><a href="<?php echo $google; ?>"><i class="fa fa-google-plus"></i></a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div><!--/header_top-->
		<div class="header-middle"><!--header-middle-->
			<div class="container">
				<div class="row">
					<div class="col-sm-4">
						<div class="logo pull-left">
							<a href="index.html"><img src="<?php echo base_url('asset/images/home/'.$logo.''); ?>" alt="<?php echo $deskripsi;?>" /></a>
						</div>
					</div>
					<div class="col-sm-8">
						<div class="shop-menu pull-right">
							<ul class="nav navbar-nav">
								<?php $menu = array('user' => 'Hubungi Kami', 'star' => 'Cara Belanja', 'shopping-cart' => 'Keranjang', 'lock' => 'Login'); 
								foreach ($menu as $key => $value) {
									echo '<li><a href="#" class="'.$value.'"><i class="fa fa-'.$key.' "></i> '.$value.'</a></li>';
								}
								?>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div><!--/header-middle-->
		<div class="header-bottom"><!--header-bottom-->
			<div class="container">
				<div class="row">
					<div class="col-sm-9">
						<div class="navbar-header">
							<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
								<span class="sr-only">Toggle navigation</span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</button>
						</div>
						<div class="mainmenu pull-left">
							<ul class="nav navbar-nav collapse navbar-collapse">
								<li><a class="home" href="#"><i class="fa fa-home"></i> Beranda</a></li>
								<li><a class="all-product" href="#"><i class="fa fa-shopping-cart"></i> Produk</a></li>
								<li><a href="#"><i class="fa fa-pencil"></i> Artikel</a></li>
								<li><a href="#"><i class="fa fa-user"></i> Testimoni</a></li>
							</ul>
						</div>
					</div>
					<div class="col-sm-3">
						<div class="search_box pull-right">
							<input type="text" placeholder="Search"/>
						</div>
					</div>
				</div>
			</div>
		</div><!--/header-bottom-->
	</header><!--/header-->
	<div id="loading" class="well"><center> <img height="50" src="<?php echo base_url('asset/images/loading.gif') ?> "> </center></div>
	<div id="main-page"></div>
	<section id="slider"><!--slider-->
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<div id="slider-carousel" class="carousel slide" data-ride="carousel">
						<ol class="carousel-indicators">
							<li data-target="#slider-carousel" data-slide-to="0" class="active"></li>
							<li data-target="#slider-carousel" data-slide-to="1"></li>
							<li data-target="#slider-carousel" data-slide-to="2"></li>
						</ol>
						<div class="carousel-inner">
							<?php 
							foreach ($banner->result() as $value) {
								$status = ($value->status==1)?'item active':'item';
								?>
								<div class="<?php echo $status; ?>">
									<div class="col-sm-6">
										<h1><span><?php echo $nama; ?></span</h1>
										<h2><?php echo $value->judul; ?></h2>
										<p><?php echo $value->deskripsi; ?> </p>
										<button type="button" class="btn btn-default get">Get it now</button>
									</div>
									<div class="col-sm-6">
										<img src="<?php echo base_url(); ?>asset/images/home/<?php echo $value->gambar; ?>" class="girl img-responsive" alt="" />
										<img src="<?php echo base_url(); ?>asset/images/home/pricing.png"  class="pricing" alt="" />
									</div>
								</div>
								<?php } ?>
							</div>
							<a href="#slider-carousel" class="left control-carousel hidden-xs" data-slide="prev">
								<i class="fa fa-angle-left"></i>
							</a>
							<a href="#slider-carousel" class="right control-carousel hidden-xs" data-slide="next">
								<i class="fa fa-angle-right"></i>
							</a>
						</div>
					</div>
				</div>
			</div>
		</section><!--/slider-->
		<section id="sidebar"> <!-- kategori -->
			<div class="container">
				<div class="row">
					<div class="col-sm-3">
						<div class="left-sidebar">
							<h2>Kategori</h2>
							<div class="panel-group category-products" id="accordian">
								<?php $no=1; foreach ($kategori->result_array() as $key => $value) { 
									$sub_kat = $this->db->get_where('toa_kategori', array('kode_level'=>1, 'kode_parent'=>$value['id_kategori']) );
									foreach ($sub_kat->result() as $row) {
										$parent = $row->kode_parent;
									}
									?>
									<div class="panel panel-default">
										<div class="panel-heading">
											<h4 class="panel-title">
												<a data-toggle="collapse" data-parent="#accordian" href="#<?php echo $no;  ?>">
													<?php if($value['id_kategori'] == $parent) { ?>
													<span class="badge pull-right"><i class="fa fa-plus"></i></span>
													<?php } ?>
													<?php echo $value['nama_kategori']; ?>
												</a>
											</h4>
										</div>
										<div id="<?php echo $no; ?>" class="panel-collapse collapse">
											<div class="panel-body">
												<?php foreach ($sub_kat->result_array() as $key2 => $value2) { ?>
												<ul>
													<li><a href="#"><?php echo $sub =  isset($value2['nama_kategori'])?$value2['nama_kategori']:'Belum Ada Sub Kategori'; ?> </a></li>
												</ul>
												<?php } ?>
											</div>
										</div>
									</div>
									<?php 
									$no++; } ?>
								</div>
								<div class="brands_products"><!--brands_products-->
									<h2>Info</h2>
									<div class="brands-name">
										<ul class="nav nav-pills nav-stacked">
											<li><a href="#"> <span class="pull-right">(50)</span>Jasa Pengiriman</a></li>
											<li><a href="#"> <span class="pull-right">(56)</span>Metode Pembayaran</a></li>
											<li><a href="#"> <span class="pull-right">(27)</span>Testimoni</a></li>
										</ul>
									</div>
								</div><!--/brands_products-->
							</div>
						</div>
						<div class="col-sm-9 padding-right">
							<div class="home-page"></div>
							<div class="features_items"><!--features_items-->
								<h2 class="title text-center">Produk Terbaru</h2>
								<?php 
								foreach ($produk->result_array() as $key => $value) { ?>
								<div class="col-sm-4">
									<div class="product-image-wrapper">
										<div class="single-products">
											<div class="productinfo text-center">
												<img alt="<?php echo  $value['nama_produk']; ?>" height="210" src="<?php echo base_url(); ?>asset/images/produk/<?php echo $value['gambar']; ?>" alt="" />
												<h2><?php echo 'Rp '.number_format($value['harga'], 2); ?></h2>
												<p><?php echo  $value['nama_produk']; ?></p>
												<a href="<?php echo base_url('web/produk_detail').'/'. $value['kode_produk']; ?>" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Beli</a>
											</div>
											<div class="product-overlay">
												<div class="overlay-content">
													<h2><?php echo 'Rp '.number_format($value['harga'], 2); ?></h2>
													<p><?php echo $value['nama_produk']; ?></p>
													<a href="<?php echo base_url('web/produk_detail').'/'. $value['kode_produk']; ?>" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Beli</a>
												</div>
											</div>
										</div>
										<div class="choose">
											<ul class="nav nav-pills nav-justified">
												<li><a href="#"><i class="fa fa-plus-square"></i>Lihat Detail</a></li>
												<li><a href="#"><i class="fa fa-plus-square"></i>Semua Produk</a></li>
											</ul>
										</div>
									</div>
								</div>
								<?php } ?>
							</div><!--features_items-->
							<div class="recommended_items"><!--recommended_items-->
								<h2 class="title text-center">Artikel Terbaru</h2>
								<div id="recommended-item-carousel" class="carousel slide" data-ride="carousel">
									<div class="carousel-inner">
										<?php 
										$rekomen =  array(
											'item active' => array(
												'recommend1' => array('$56','Easy Polo Black Edition'),
												'recommend2' => array('$56','Easy Polo Black Edition'),
												'recommend3' => array('$56','Easy Polo Black Edition'),
												), 
											'item' => array(
												'recommend1'=> array('$56','Easy Polo Black Edition'),
												'recommend2'=> array('$56','Easy Polo Black Edition'),
												'recommend3'=> array('$56','Easy Polo Black Edition'),
												) 
											); 
										foreach ($rekomen as $key => $value) {
											?>
											<div class="<?php echo $key; ?>">	
												<?php foreach ($value as $key2 => $value2) { ?>
												<div class="col-sm-4">
													<div class="product-image-wrapper">
														<div class="single-products">
															<div class="productinfo text-center">
																<img src="<?php echo base_url(); ?>asset/images/home/<?php echo $key2; ?>.jpg" alt="" />
																<h2><?php echo $value2[0]; ?></h2>
																<p><?php echo $value2[1]; ?></p>
																<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
															</div>
														</div>
													</div>
												</div>
												<?php } ?>
											</div>
											<?php } ?>
										</div>
										<a class="left recommended-item-control" href="#recommended-item-carousel" data-slide="prev">
											<i class="fa fa-angle-left"></i>
										</a>
										<a class="right recommended-item-control" href="#recommended-item-carousel" data-slide="next">
											<i class="fa fa-angle-right"></i>
										</a>			
									</div>
								</div><!--/recommended_items-->
							</div>
						</div>
					</div>
				</section>
				<footer id="footer"><!--Footer-->
					<div class="footer-widget">
						<div class="container">
							<div class="row">
								<div class="col-sm-4">
									<div class="single-widget">
										<h2>Informasi</h2>
										<ul class="nav nav-pills nav-stacked">
											<li><a href="#"><i class="fa fa-star"></i>Cara Belanja</a></li>
											<li><a href="#"><i class="fa fa-star"></i>Layanan dan Kebijakan Privasi</a></li>
											<li><a href="#"><i class="fa fa-star"></i>Pengiriman dan Retur Produk</a></li>
										</ul>
									</div>
								</div>
								<div class="col-sm-4">
									<div class="single-widget">
										<h2>Selalu Terhubung</h2>
										<ul class="nav nav-pills nav-stacked">
											<li><a href="#"><i class="fa fa-plus"></i> Facebook</i></a></li>
											<li><a href="#"><i class="fa fa-plus"></i> Twitter</a></li>
											<li><a href="#"><i class="fa fa-plus"></i> Google</a></li>
										</ul>
									</div>
								</div>
								<div class="col-sm-4">
									<div class="single-widget">
										<h2>Blog</h2>
										<ul class="nav nav-pills nav-stacked">
											<li><a href="#"><i class="fa fa-pencil"></i>Artikel Terbaru</a></li>
											<li><a href="#"><i class="fa fa-pencil"></i>Artikel Terpopuler</a></li>
											<li><a href="#"><i class="fa fa-pencil"></i>Testimoni</a></li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="footer-bottom">
						<div class="container">
							<div class="row">
								<p class="pull-left">Copyright © 2015 | <a href="">Emade Haryo Kuncoro</a></p>
								<p class="pull-right">Designed by <span><a target="_blank" href="http://www.themeum.com">Themeum</a></span> Customed by <a href="">EHaKu</a></p>
							</div>
						</div>
					</div>
				</footer><!--/Footer-->
			</body>
			</html>
			<script type="text/javascript">
			$(document).ready(function() {
				$('#loading').hide();
				login();
				Keranjang();
				Home();
				produk();
			});
			function produk()
			{
				var base_url = '<?php echo base_url(); ?>';
				$('.all-product').click(function() {
					$('#slider').hide();
					$('.recommended_items').hide();
					$('#main-page').html('');
					$('a:parent').removeClass('active');
					$('.all-product:parent').addClass('active');
					$.ajax({
						url:base_url+"web/produk",
						async:false,
						type:'post',
						data:{'<?php echo $this->security->get_csrf_token_name(); ?>':'<?php echo $this->security->get_csrf_hash(); ?>'},
						dataType:'html',
						beforeSend:function(){
							$('#loading').show();
						},
						success:function(data){
							$('#loading').hide();
							$('.features_items').show();
							$('.features_items').html(data);
							$('#slider, #sidebar').show();
						}
					})
				});
			}
			function login()
			{
				var base_url = '<?php echo base_url(); ?>';
				$('.Login').click(function() {
					$('#slider, #sidebar').hide();
					$('a:parent').removeClass('active');
					$('.Login:parent').addClass('active');
					$.ajax({
						url:base_url+"web/login",
						async:false,
						type:'post',
						data:{'<?php echo $this->security->get_csrf_token_name(); ?>':'<?php echo $this->security->get_csrf_hash(); ?>'},
						dataType:'html',
						beforeSend:function(){
							$('#loading').show();
						},
						success:function(data){
							$('#loading').hide();
							$('#main-page').html(data);
						}
					})
				});
			}
			function Keranjang()
			{
				var base_url = '<?php echo base_url(); ?>';
				$('.Keranjang').click(function() {
					$('#slider, #sidebar').hide();
					$('a:parent').removeClass('active');
					$('.Keranjang:parent').addClass('active');
					$.ajax({
						url:base_url+"web/Keranjang",
						async:false,
						type:'post',
						data:{'<?php echo $this->security->get_csrf_token_name(); ?>':'<?php echo $this->security->get_csrf_hash(); ?>'},
						dataType:'html',
						beforeSend:function(){
							$('#loading').show();
						},
						success:function(data){
							$('#loading').hide();
							$('#main-page').html(data);
						}
					})
				});
			}
			function Home()
			{
				var base_url = '<?php echo base_url(); ?>';
				$('.home').click(function() {
					$('.home-page').html('');
					$('a:parent').removeClass('active');
					$('.home:parent').addClass('active');
					$('#main-page').html('');
					$.ajax({
						url:base_url+"web/home",
						async:false,
						type:'post',
						data:{'<?php echo $this->security->get_csrf_token_name(); ?>':'<?php echo $this->security->get_csrf_hash(); ?>'},
						dataType:'html',
						beforeSend:function(){
							$('#loading').show();
						},
						success:function(data){
							$('#loading').hide();
							$('.home-page').html(data);
							$('#slider, #sidebar').show();
						}
					})
				});
			}
			</script>