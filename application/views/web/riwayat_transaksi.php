<section id="cart_items">
	<div class="container">
		<div class="breadcrumbs">
			<ol class="breadcrumb">
				<li><a href="<?php echo base_url(); ?>">Home</a></li>
				<li><a href="<?php echo base_url('user'); ?>">Member</a></li>
				<li class="active">Semua Transaksi</li>
			</ol>
		</div>
		<div class="alert alert-warning">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			<strong>Status</strong> <br>
			1. Pending 		: Pesanan menunggu proses konfirmasi. <br>
			2. Belum Bayar 	: Pesanan sudah dikonfirmasi, email pemberitahuan pembayaran sudah dikirim. Pemesan belum mengkonfirmasi pembayaran. <br>
			3. Lunas 		: Pesanan sudah dikonfirmasi dan dibayar. Barang dalam proses pengiriman. <br>
			4. Terkirim 	: Pesanan sudah dikonfirmasi dan dibayar. Barang sudah dikirim. Anda akan mendapat email yang berisi resi pengiriman.
		</div>
		<div class="panel panel-default">
			<div class="panel-heading"><h3><i class="fa fa-bookmark"></i> Semua Transaksi</h3></div>
			<div class="panel-body text-danger">
				<?php if($order->num_rows() == 0 ) {echo 'Belum Ada Transaksi';} else { ?>
				<?php echo $msg = $this->session->flashdata('result')?'<div class="alert alert-warning">'.$this->session->flashdata('result').'</div>':''; ?>
				<table class="table table-hover">
					<thead>
						<tr>
							<th>No</th>
							<th>Tanggal Pesan</th>
							<th>Total Bayar</th>
							<th>Status</th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						<?php $no=1; foreach ($order->result() as $value) { ?>
						<tr>
							<td><?php echo $no; ?></td>
							<td><?php echo $this->Adminmodel->ubahTanggal($value->tgl_order); ?></td>
							<td><?php echo $tb =  ($value->total_bayar != 0) ? 'Rp '. number_format($value->total_bayar, 2):'-'; ?></td>
							<td><?php echo $value->status; ?></td>
							<td>
								<a class="btn btn-danger" href="<?php echo base_url('user/detail_pembayaran').'/'.$value->kode_order; ?>"><i class="fa fa-star"></i> Detail Transaksi</a>
							</td>
						</tr>
						<?php $no++; } ?>
					</tbody>
				</table>
				<?php } ?>
			</div>
			<div class="panel-footer"></div>
		</div>
	</section>
