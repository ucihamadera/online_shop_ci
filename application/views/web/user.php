<section id="cart_items">
	<div class="container">
		<div class="breadcrumbs">
			<ol class="breadcrumb">
				<li><a href="<?php echo base_url(); ?>">Home</a></li>
				<li class="active">Halaman Member</li>
			</ol>
		</div>
		<div class="alert alert-warning">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			<strong>Tips :</strong><br>
			1. Pengaturan profil merupakan menu untuk mengubah password/profil anda. <br>
			2. Jika anda sudah selesai berbelanja, silakan konfirmasi pembayaran anda melalui menu Konfirmasi Pembayaran. <br>
			3. Anda dapat melihat riwayat transaksi dan status transaksi anda pada menu Semua Transaksi. 
		</div>
		<div class="panel panel-default">
			<div class="panel-heading">
				<h4>Selamat Datang, <?php $nm = explode('@', $this->session->userdata('email')); echo $nm[0]; ?></h4>
			</div>
			<div class="panel-body text-center">
				<div class="col-sm-3">
					<a href="<?php echo base_url('user/pengaturan'); ?>">
						<h1><i style="font-size:50px;" class="fa fa-user"></i></h1>
						<b>Pengaturan Profil</b>
					</a>
				</div>
				<div class="col-sm-3">
					<a href="<?php echo base_url('user/konfirmasi_pembayaran'); ?>">
						<h1><i style="font-size:50px;" class="fa fa-shopping-cart"></i></h1>
						<b>Konfirmasi Pembayaran</b>
					</a>
				</div>
				<div class="col-sm-3">
					<a href="<?php echo base_url('user/riwayat_transaksi'); ?>">
						<h1><i style="font-size:50px;" class="fa fa-star"></i></h1>
						<b>Semua Transaksi</b>
					</a>
				</div>
				<div class="col-sm-3">
					<a href="<?php echo base_url('user/logout'); ?>">
						<h1><i style="font-size:50px;" class="fa fa-sign-out"></i></h1>
						<b>Keluar</b>
					</a>
				</div>
			</div>
			<div class="panel-footer"></div>
		</div>
	</section>