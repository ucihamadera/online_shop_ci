<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="<?php echo $nama .'|'. $deskripsi; ?>">
	<meta name="author" content="Emade Haryo Kuncoro">
	<title>Home | E-Shopper</title>
	<link href="<?php echo base_url(); ?>asset/css/bootstrap.min.css" rel="stylesheet">
	<link href="<?php echo base_url(); ?>asset/css/font-awesome.min.css" rel="stylesheet">
	<link href="<?php echo base_url(); ?>asset/css/prettyPhoto.css" rel="stylesheet">
	<link href="<?php echo base_url(); ?>asset/css/price-range.css" rel="stylesheet">
	<link href="<?php echo base_url(); ?>asset/css/animate.css" rel="stylesheet">
	<link href="<?php echo base_url(); ?>asset/css/main.css" rel="stylesheet">
	<link href="<?php echo base_url(); ?>asset/css/responsive.css" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="<?php echo base_url(); ?>asset/images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url(); ?>asset/images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url(); ?>asset/images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url(); ?>asset/images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="<?php echo base_url(); ?>asset/images/ico/apple-touch-icon-57-precomposed.png">
</head><!--/head-->

<body>
	<header id="header"><!--header-->
		<div class="header_top"><!--header_top-->
			<div class="container">
				<div class="row">
					<div class="col-sm-6">
						<div class="contactinfo">
							<ul class="nav nav-pills">
								<li><a href="#"><i class="fa fa-phone"></i> <?php echo $no_telp; ?></a></li>
								<li><a href="#"><i class="fa fa-envelope"></i> <?php echo $email; ?></a></li>
							</ul>
						</div>
					</div>
					<div class="col-sm-6">
						<div class="social-icons pull-right">
							<ul class="nav navbar-nav">
								<li><a href="<?php echo $fb; ?>"><i class="fa fa-facebook"></i></a></li>
								<li><a href="<?php echo $twitter; ?>"><i class="fa fa-twitter"></i></a></li>
								<li><a href="<?php echo $google; ?>"><i class="fa fa-google-plus"></i></a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div><!--/header_top-->
		
		<div class="header-middle"><!--header-middle-->
			<div class="container">
				<div class="row">
					<div class="col-sm-4">
						<div class="logo pull-left">
							<a href="index.html"><img src="<?php echo base_url('asset/images/home/'.$logo.''); ?>" alt="<?php echo $deskripsi;?>" /></a>
						</div>
					</div>
					<div class="col-sm-8">
						<div class="shop-menu pull-right">
							<ul class="nav navbar-nav">
								<?php $menu = array('user' => 'Tentang Kami', 'star' => 'Cara Belanja', 'shopping-cart' => 'Keranjang Belanja', 'lock' => 'Login'); 
								foreach ($menu as $key => $value) {
									echo '<li><a href="#"><i class="fa fa-'.$key.'"></i> '.$value.'</a></li>';
								}
								?>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div><!--/header-middle-->

		<div class="header-bottom"><!--header-bottom-->
			<div class="container">
				<div class="row">
					<div class="col-sm-9">
						<div class="navbar-header">
							<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
								<span class="sr-only">Toggle navigation</span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</button>
						</div>
						<div class="mainmenu pull-left">
							<ul class="nav navbar-nav collapse navbar-collapse">
								<li><a href="#"><i class="glyphicon glyphicon-home"></i> Beranda</a></li>
								<li><a href="#">Produk</a></li>
								<li><a href="#">Artikel</a></li>
								<li><a data-toggle="modal" href='#modal_tk' href="#">Hubungi Kami</a></li>
							</ul>
						</div>
					</div>
					<div class="col-sm-3">
						<div class="search_box pull-right">
							<input type="text" placeholder="Search"/>
						</div>
					</div>
				</div>
			</div>
		</div><!--/header-bottom-->
	</header><!--/header-->

	<section id="slider"><!--slider-->
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<div id="slider-carousel" class="carousel slide" data-ride="carousel">
						<ol class="carousel-indicators">
							<li data-target="#slider-carousel" data-slide-to="0" class="active"></li>
							<li data-target="#slider-carousel" data-slide-to="1"></li>
							<li data-target="#slider-carousel" data-slide-to="2"></li>
						</ol>

						<div class="carousel-inner">
							<?php 

							foreach ($banner->result() as $value) {
								$status = ($value->status==1)?'item active':'item';
								?>
								<div class="<?php echo $status; ?>">
									<div class="col-sm-6">
										<h1><span><?php echo $nama; ?></span</h1>
										<h2><?php echo $value->judul; ?></h2>
										<p><?php echo $value->deskripsi; ?> </p>
										<button type="button" class="btn btn-default get">Get it now</button>
									</div>
									<div class="col-sm-6">
										<img src="<?php echo base_url(); ?>asset/images/home/<?php echo $value->gambar; ?>" class="girl img-responsive" alt="" />
										<img src="<?php echo base_url(); ?>asset/images/home/pricing.png"  class="pricing" alt="" />
									</div>
								</div>

								<?php } ?>
								

								

							</div>

							<a href="#slider-carousel" class="left control-carousel hidden-xs" data-slide="prev">
								<i class="fa fa-angle-left"></i>
							</a>
							<a href="#slider-carousel" class="right control-carousel hidden-xs" data-slide="next">
								<i class="fa fa-angle-right"></i>
							</a>
						</div>

					</div>
				</div>
			</div>
		</section><!--/slider-->

		<section>
			<div class="container">
				<div class="row">
					<div class="col-sm-3">
						<div class="left-sidebar">
							<h2>Category</h2>
							<div class="panel-group category-products" id="accordian"><!--category-productsr-->
								<div class="panel panel-default">
									<div class="panel-heading">
										<h4 class="panel-title">
											<a data-toggle="collapse" data-parent="#accordian" href="#sportswear">
												<span class="badge pull-right"><i class="fa fa-plus"></i></span>
												Sportswear
											</a>
										</h4>
									</div>
									<div id="sportswear" class="panel-collapse collapse">
										<div class="panel-body">
											<ul>
												<li><a href="#">Nike </a></li>
												<li><a href="#">Under Armour </a></li>
												<li><a href="#">Adidas </a></li>
												<li><a href="#">Puma</a></li>
												<li><a href="#">ASICS </a></li>
											</ul>
										</div>
									</div>
								</div>
								<div class="panel panel-default">
									<div class="panel-heading">
										<h4 class="panel-title">
											<a data-toggle="collapse" data-parent="#accordian" href="#mens">
												<span class="badge pull-right"><i class="fa fa-plus"></i></span>
												Mens
											</a>
										</h4>
									</div>
									<div id="mens" class="panel-collapse collapse">
										<div class="panel-body">
											<ul>
												<li><a href="#">Fendi</a></li>
												<li><a href="#">Guess</a></li>
												<li><a href="#">Valentino</a></li>
												<li><a href="#">Dior</a></li>
												<li><a href="#">Versace</a></li>
												<li><a href="#">Armani</a></li>
												<li><a href="#">Prada</a></li>
												<li><a href="#">Dolce and Gabbana</a></li>
												<li><a href="#">Chanel</a></li>
												<li><a href="#">Gucci</a></li>
											</ul>
										</div>
									</div>
								</div>

								<div class="panel panel-default">
									<div class="panel-heading">
										<h4 class="panel-title">
											<a data-toggle="collapse" data-parent="#accordian" href="#womens">
												<span class="badge pull-right"><i class="fa fa-plus"></i></span>
												Womens
											</a>
										</h4>
									</div>
									<div id="womens" class="panel-collapse collapse">
										<div class="panel-body">
											<ul>
												<li><a href="#">Fendi</a></li>
												<li><a href="#">Guess</a></li>
												<li><a href="#">Valentino</a></li>
												<li><a href="#">Dior</a></li>
												<li><a href="#">Versace</a></li>
											</ul>
										</div>
									</div>
								</div>
								<?php $category = array('Kids','Fashion','Households','Interiors','Clothing','Bags','Shoes'); 
								foreach ($category as $value) {
									?>
									<div class="panel panel-default">
										<div class="panel-heading">
											<h4 class="panel-title"><a href="#"><?php echo $value; ?></a></h4>
										</div>
									</div>
									<?php } ?>
								</div><!--/category-products-->

								<div class="brands_products"><!--brands_products-->
									<h2>Brands</h2>
									<div class="brands-name">
										<ul class="nav nav-pills nav-stacked">
											<li><a href="#"> <span class="pull-right">(50)</span>Acne</a></li>
											<li><a href="#"> <span class="pull-right">(56)</span>Grüne Erde</a></li>
											<li><a href="#"> <span class="pull-right">(27)</span>Albiro</a></li>
											<li><a href="#"> <span class="pull-right">(32)</span>Ronhill</a></li>
											<li><a href="#"> <span class="pull-right">(5)</span>Oddmolly</a></li>
											<li><a href="#"> <span class="pull-right">(9)</span>Boudestijn</a></li>
											<li><a href="#"> <span class="pull-right">(4)</span>Rösch creative culture</a></li>
										</ul>
									</div>
								</div><!--/brands_products-->

								<div class="price-range"><!--price-range-->
									<h2>Price Range</h2>
									<div class="well text-center">
										<input type="text" class="span2" value="" data-slider-min="0" data-slider-max="600" data-slider-step="5" data-slider-value="[250,450]" id="sl2" ><br />
										<b class="pull-left">$ 0</b> <b class="pull-right">$ 600</b>
									</div>
								</div><!--/price-range-->



							</div>
						</div>

						<div class="col-sm-9 padding-right">
							<div class="features_items"><!--features_items-->
								<h2 class="title text-center">Features Items</h2>
								<?php 
								$items = array(
									'product1'=> array ( 'harga'=>'$56', 'details'=>'Easy Polo Black Edition', 'link'=>'#'),
									'product2'=> array ( 'harga'=>'$56', 'details'=>'Easy Polo Black Edition', 'link'=>'#'),
									'product3'=> array ( 'harga'=>'$56', 'details'=>'Easy Polo Black Edition', 'link'=>'#'),
									'product4'=> array ( 'harga'=>'$56', 'details'=>'Easy Polo Black Edition', 'link'=>'#'),
									'product5'=> array ( 'harga'=>'$56', 'details'=>'Easy Polo Black Edition', 'link'=>'#'),
									'product6'=> array ( 'harga'=>'$56', 'details'=>'Easy Polo Black Edition', 'link'=>'#')
									); 
								foreach ($items as $key => $value) {

									?>
									<div class="col-sm-4">
										<div class="product-image-wrapper">
											<div class="single-products">
												<div class="productinfo text-center">
													<img src="<?php echo base_url(); ?>asset/images/home/<?php echo $key; ?>.jpg" alt="" />
													<h2><?php echo $value['harga']; ?></h2>
													<p><?php echo $value['details']; ?></p>
													<a href="<?php echo $value['link']; ?>" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
												</div>
												<div class="product-overlay">
													<div class="overlay-content">
														<h2><?php echo $value['harga']; ?></h2>
														<p><?php echo $value['details']; ?></p>
														<a href="<?php echo $value['link']; ?>" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
													</div>
												</div>
											</div>
											<div class="choose">
												<ul class="nav nav-pills nav-justified">
													<li><a href="#"><i class="fa fa-plus-square"></i>Add to wishlist</a></li>
													<li><a href="#"><i class="fa fa-plus-square"></i>Add to compare</a></li>
												</ul>
											</div>
										</div>
									</div>
									<?php } ?>

								</div><!--features_items-->


								<div class="recommended_items"><!--recommended_items-->
									<h2 class="title text-center">recommended items</h2>

									<div id="recommended-item-carousel" class="carousel slide" data-ride="carousel">
										<div class="carousel-inner">
											<?php 
											$rekomen =  array(
												'item active' => array(
													'recommend1' => array('$56','Easy Polo Black Edition'),
													'recommend2' => array('$56','Easy Polo Black Edition'),
													'recommend3' => array('$56','Easy Polo Black Edition')
													), 
												'item' => array(
													'recommend1'=> array('$56','Easy Polo Black Edition'),
													'recommend2'=> array('$56','Easy Polo Black Edition'),
													'recommend3'=> array('$56','Easy Polo Black Edition')
													) 
												); 
											foreach ($rekomen as $key => $value) {

												?>
												<div class="<?php echo $key; ?>">	
													<?php foreach ($value as $key2 => $value2) { ?>
													<div class="col-sm-4">
														<div class="product-image-wrapper">
															<div class="single-products">
																<div class="productinfo text-center">
																	<img src="<?php echo base_url(); ?>asset/images/home/<?php echo $key2; ?>.jpg" alt="" />
																	<h2><?php echo $value2[0]; ?></h2>
																	<p><?php echo $value2[1]; ?></p>

																	<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
																</div>

															</div>
														</div>
													</div>
													<?php } ?>
												</div>
												<?php } ?>
											</div>
											<a class="left recommended-item-control" href="#recommended-item-carousel" data-slide="prev">
												<i class="fa fa-angle-left"></i>
											</a>
											<a class="right recommended-item-control" href="#recommended-item-carousel" data-slide="next">
												<i class="fa fa-angle-right"></i>
											</a>			
										</div>
									</div><!--/recommended_items-->

								</div>
							</div>
						</div>
					</section>

					<footer id="footer"><!--Footer-->


						<div class="footer-widget">
							<div class="container">
								<div class="row">
									<div class="col-sm-2">
										<div class="single-widget">
											<h2>Service</h2>
											<ul class="nav nav-pills nav-stacked">
												<li><a href="#">Online Help</a></li>
												<li><a href="#">Contact Us</a></li>
												<li><a href="#">Order Status</a></li>
												<li><a href="#">Change Location</a></li>
												<li><a href="#">FAQ’s</a></li>
											</ul>
										</div>
									</div>
									<div class="col-sm-2">
										<div class="single-widget">
											<h2>Quock Shop</h2>
											<ul class="nav nav-pills nav-stacked">
												<li><a href="#">T-Shirt</a></li>
												<li><a href="#">Mens</a></li>
												<li><a href="#">Womens</a></li>
												<li><a href="#">Gift Cards</a></li>
												<li><a href="#">Shoes</a></li>
											</ul>
										</div>
									</div>
									<div class="col-sm-2">
										<div class="single-widget">
											<h2>Policies</h2>
											<ul class="nav nav-pills nav-stacked">
												<li><a href="#">Terms of Use</a></li>
												<li><a href="#">Privecy Policy</a></li>
												<li><a href="#">Refund Policy</a></li>
												<li><a href="#">Billing System</a></li>
												<li><a href="#">Ticket System</a></li>
											</ul>
										</div>
									</div>
									<div class="col-sm-2">
										<div class="single-widget">
											<h2>About Shopper</h2>
											<ul class="nav nav-pills nav-stacked">
												<li><a href="#">Company Information</a></li>
												<li><a href="#">Careers</a></li>
												<li><a href="#">Store Location</a></li>
												<li><a href="#">Affillate Program</a></li>
												<li><a href="#">Copyright</a></li>
											</ul>
										</div>
									</div>
									<div class="col-sm-3 col-sm-offset-1">
										<div class="single-widget">
											<h2>About Shopper</h2>
											<form action="#" class="searchform">
												<input type="text" placeholder="Your email address" />
												<button type="submit" class="btn btn-default"><i class="fa fa-arrow-circle-o-right"></i></button>
												<p>Get the most recent updates from <br />our site and be updated your self...</p>
											</form>
										</div>
									</div>

								</div>
							</div>
						</div>

						<div class="footer-bottom">
							<div class="container">
								<div class="row">
									<p class="pull-left">Copyright © 2015 | <a href="">Emade Haryo Kuncoro</a></p>
									<p class="pull-right">Designed by <span><a target="_blank" href="http://www.themeum.com">Themeum</a></span> Customed by <a href="">EHaKu</a></p>
								</div>
							</div>
						</div>

					</footer><!--/Footer-->
<a class="btn btn-primary" data-toggle="modal" href='#modal_tk'>Trigger modal</a>

					<?php $this->load->view('web/modal/tentang_kami'); ?>


					<script src="<?php echo base_url(); ?>asset/js/jquery.js"></script>
					<script src="<?php echo base_url(); ?>asset/js/bootstrap.min.js"></script>
					<script src="<?php echo base_url(); ?>asset/js/jquery.scrollUp.min.js"></script>
					<script src="<?php echo base_url(); ?>asset/js/price-range.js"></script>
					<script src="<?php echo base_url(); ?>asset/js/jquery.prettyPhoto.js"></script>
					<script src="<?php echo base_url(); ?>asset/js/main.js"></script>
				</body>
				</html>