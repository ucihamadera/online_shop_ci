<ol class="breadcrumb">
	<li>
		<a href="<?php echo base_url('webmaster'); ?>">Home</a>
	</li>
	<li class="active">Pengaturan Halaman Aturan Retur Produk</li>
</ol>

<?php echo $msg = $this->session->flashdata('result')?'<div class="alert alert-info">'.$this->session->flashdata('result').'</div>':''; ?>
<?php echo $error =  validation_errors() ? '<div class="alert alert-warning">'.validation_errors().'</div>':''; ?>

<?php echo form_open('webmaster/simpan_retur_produk', 'role="form"'); ?>
	<legend><span class="glyphicon glyphicon-shopping-cart"></span> Aturan Retur Produk</legend>
	<input type="hidden" name="kode" value="<?php echo $kode= isset($kode) ? $kode:''; ?>">
	<div class="form-group">
		<label for="">Nama Halaman</label>
		<input type="text" readonly class="form-control" value="Aturan Retur Produk">
	</div>
	<div class="form-group">
		<label for="">Isi Halaman</label>
		<textarea name="deskripsi"><?php echo $deskripsi= isset($deskripsi) ? $deskripsi:''; ?></textarea>
	</div>

	<button type="submit" class="btn btn-primary">Simpan Data</button>
	<a href="<?php echo base_url('webmaster/profil'); ?>" class="btn btn-danger">Kembali</a>
<?php echo form_close(); ?>



