<ol class="breadcrumb">
	<li>
		<a href="<?php echo base_url('webmaster'); ?>">Home</a>
	</li>
	
	<li class="active">Pengaturan Info</li>
</ol>
<?php echo form_open_multipart('webmaster/update_info', 'role="form"'); ?>
<?php echo $msg = $this->session->flashdata('result')?'<div class="alert alert-info">'.$this->session->flashdata('result').'</div>':''; ?>
<?php echo $error =  validation_errors() ? '<div class="alert alert-warning">'.validation_errors().'</div>':''; ?>
<?php echo $upload_error =  $this->upload->display_errors() ? '<div class="alert alert-warning">'.$this->upload->display_errors().'</div>':''; ?>
	<legend><span class="glyphicon glyphicon-shopping-cart"></span> Pengaturan Info Toko</legend>
	<input type="hidden" name="kode" value="<?php echo $kode = isset($kode) ? $kode:''; ?>">
	<div class="form-group">
		<label for="">Nama Toko</label>
		<input type="text" class="form-control"  name="nama" placeholder="Masukan Nama Toko" value="<?php echo $nama= isset($nama) ? $nama:''; ?>">
	</div>
	<div class="form-group">
		<label for="">Deskripsi</label>
		<input type="text" class="form-control"  name="deskripsi" placeholder="Masukan Deskripsi" value="<?php echo $deskripsi= isset($deskripsi) ? $deskripsi:''; ?>">
	</div>
	<div class="form-group">
		<label for="">Kata Kunci</label>
		<input type="text" class="form-control"  name="keywords" placeholder="Masukan Kata Kunci, Batasi dengan koma" value="<?php echo $keywords= isset($keywords) ? $keywords:''; ?>">
	</div>
	<div class="form-group">
		<label for="">Alamat</label>
		<input type="text" class="form-control"  name="alamat" placeholder="Masukan Alamat" value="<?php echo $alamat= isset($alamat) ? $alamat:''; ?>">
	</div>
	<div class="form-group">
		<label for="">No Telp</label>
		<input type="text" class="form-control"  name="no_telp" placeholder="Masukan No Telp" value="<?php echo $no_telp= isset($no_telp) ? $no_telp:''; ?>">
	</div>
	<div class="form-group">
		<label for="">Email</label>
		<input type="text" class="form-control"  name="email" placeholder="Masukan email" value="<?php echo $email= isset($email) ? $email:''; ?>">
	</div>
	<div class="form-group">
		<label for="">Akun Google</label>
		<input type="text" class="form-control"  name="google" placeholder="Masukan url Akun Google anda" value="<?php echo $google= isset($google) ? $google:''; ?>">
	</div>
	<div class="form-group">
		<label for="">Facebook</label>
		<input type="text" class="form-control"  name="fb" placeholder="Masukan url Akun Facebook anda" value="<?php echo $fb= isset($fb) ? $fb:''; ?>">
	</div>
	<div class="form-group">
		<label for="">Twitter</label>
		<input type="text" class="form-control"  name="twitter" placeholder="Masukan url Akun Twitter anda" value="<?php echo $twitter= isset($twitter) ? $twitter:''; ?>">
	</div>
	<div class="form-group">
		<label for="">Logo</label>
		<br/>
		<img class="col-md-2 pull-right img-responsive" alt="logo-toko" src="<?php echo base_url('asset/images/home/'.$logo.''); ?>">
		<input type="file" name="logo">
		<input type="hidden" name="nama_logo" value="<?php echo $twitter= isset($logo) ? $logo:''; ?>">
		<p class="help-block">Silakan Pilih logo untuk toko online.</p>
	</div>
	
	<button type="submit" class="btn btn-primary">Simpan Data</button>
	<a href="<?php echo base_url('webmaster'); ?>" class="btn btn-danger">Kembali</a>
<?php echo form_close(); ?>
