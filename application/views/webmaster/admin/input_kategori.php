<ol class="breadcrumb">
	<li>
		<a href="<?php echo base_url('webmaster'); ?>">Home</a>
	</li>
	<li>
		<a href="<?php echo base_url('webmaster/kategori'); ?>">kategori</a>
	</li>
	<li class="active">Tambah Data</li>
</ol>


<?php echo form_open('webmaster/simpan_kategori', 'role="form"'); ?>
<?php echo $error =  validation_errors() ? '<div class="alert alert-warning">'.validation_errors().'</div>':''; ?>
	<legend><span class="glyphicon glyphicon-th-list"></span> Tambah Data kategori</legend>
<div class="alert alert-info">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	<strong>Tips : </strong> Pilih opsi <strong>Induk Kategori</strong> Jika ingin membuat kategori baru.
</div>
	<input type="hidden" name="kode" value="<?php echo $kode_kategori = isset($kode) ? $kode:''; ?>">
	<div class="form-group">
		<label for="">Kategori</label>
		<select name="kode_parent" class="form-control">
			<option value="0">Induk Kategori</option>
			<?php 

			foreach ($kategori->result_array() as $key => $value) { 

				if($value['id_kategori'] == $parent)
				{
					echo '<option value="'.$value['id_kategori'].'" selected>'.$value['nama_kategori'].'</option>';
				}
				else
				{
					echo '<option value="'.$value['id_kategori'].'">'.$value['nama_kategori'].'</option>';
				}
			}
				
				?>

		</select>

	</div>
	<div class="form-group">
		<label for="">Nama Kategori</label>
		<input type="text" class="form-control"  required name="nama_kategori" placeholder="Masukan Nama Kategori" value="<?php echo $nama_kategori= isset($nama) ? $nama:''; ?>">
	</div>
	

	<input type="hidden" name="st" value="<?php echo $status = isset($st) ? $st:'tambah'; ?>">
	<button type="submit" class="btn btn-primary">Simpan Data</button>
	<a href="<?php echo base_url('webmaster/kategori'); ?>" class="btn btn-danger">Kembali</a>
<?php echo form_close(); ?>