<ol class="breadcrumb">
	<li>
		<a href="<?php echo base_url('webmaster'); ?>">Beranda</a>
	</li>
	<li class="active">Profil</li>
	<li><a id="tambah" href="<?php echo base_url('webmaster/tambah_profil');?>">Tambah Data</a></li>
</ol>

<?php 
echo $msg = $this->session->flashdata('result')?'<div class="alert alert-info">'.$this->session->flashdata('result').'</div>':'';
?>

<table class="table table-bordered table-striped">
	<thead>
		<tr>
			<th>No</th>
			<th>Nama</th>
			<th>Status</th>
			<th>Aksi</th>
		</tr>
	</thead>
	<tbody>
		<?php 

		echo $info = ($profil->num_rows()) >0 ? '':'<tr><td colspan="4"><center>Belum Ada Data</center></td></tr>';

		$no = $tot+1;
		foreach ($profil->result_array() as $key => $value) {

			?>
			<tr>
				<td><?php echo $no; ?></td>
				<td><?php echo $value['nama_admin']; ?></td>
				<td><?php echo $status = $value['status_admin'] == 1 ? 'Aktif':'Tidak Aktif'; ?></td>
				<td>
					<div class="btn-group">
						<a class="btn dropdown-toggle btn-primary" data-toggle="dropdown" href="#">Pilihan <span class="caret"></span></a>
						<ul class="dropdown-menu">
							<li><a href="<?php echo base_url('webmaster/edit_profil').'/'.$value['kode_admin']; ?>"><span class="glyphicon glyphicon-pencil"></span> Edit</a></li>
							<li><a onClick="return confirm('Anda yakin ingin menghapus data ini ?');" href="<?php echo base_url('webmaster/hapus_profil').'/'.$value['kode_admin']; ?>"><span class="glyphicon glyphicon-trash"></span> Hapus</a></li>
						</ul>
					</div>
				</td>
			</tr>
			<?php $no++; } ?>
		</tbody>
	</table>

	<div class="pagination"><?php echo $paginator; ?></div>
	