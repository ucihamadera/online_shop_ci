<ol class="breadcrumb">
	<li>
		<a href="<?php echo base_url('webmaster'); ?>">Beranda</a>
	</li>
	<li><a href="<?php echo base_url('webmaster/kategori') ?>">Kategori</a></li>
	<li><a id="tambah" href="<?php echo base_url('webmaster/tambah_kategori');?>">Tambah Data</a></li>
</ol>
<div class="alert alert-warning">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	<strong>Peringatan : </strong> Jangan menghapus induk kategori yang memiliki sub kategori, karena dapat menghilangkan data sub kategori.
</div>
<?php 
echo $msg = $this->session->flashdata('result')?'<div class="alert alert-info">'.$this->session->flashdata('result').'</div>':'';
?>

<table class="table table-bordered table-striped">
	
	<tbody>
		<?php 

		echo $info = ($kategori->num_rows()) >0 ? '':'<tr><td colspan="3"><center>Belum Ada Data</center></td></tr>';

		$no = 1;
		foreach ($kategori->result_array() as $key => $value) {

			?>
			<tr>
				<td><?php echo $no; ?></td>
				<td><strong>Kategori</strong></td>
				<td>
					<?php echo '<strong>'. $value['nama_kategori']. '</strong>'  ; ?>
					<div class="btn-group pull-right">
						<a class="btn dropdown-toggle btn-primary" data-toggle="dropdown" href="#">Pilihan <span class="caret"></span></a>
						<ul class="dropdown-menu">
							<li><a href="<?php echo base_url('webmaster/edit_kategori').'/'.$value['id_kategori']; ?>"><span class="glyphicon glyphicon-pencil"></span> Edit</a></li>
							<li><a onClick="return confirm('Anda yakin ingin menghapus data ini ?');" href="<?php echo base_url('webmaster/hapus_kategori').'/'.$value['id_kategori']; ?>"><span class="glyphicon glyphicon-trash"></span> Hapus</a></li>
						</ul>
					</div>
				</td>
				<?php 
				$sub_kat = $this->db->get_where('toa_kategori', array('kode_level'=>1, 'kode_parent'=>$value['id_kategori']));
				foreach ($sub_kat->result_array() as $key2 => $value2) {
					
				?>
				<tr>
					<td></td>
					<td><code>Sub Kategori -> </code></td>
					<td>
						<?php echo '<code>'. $value2['nama_kategori']. '</code>' ; ?>
						<div class="btn-group pull-right">
						<a class="btn dropdown-toggle btn-primary" data-toggle="dropdown" href="#">Pilihan <span class="caret"></span></a>
						<ul class="dropdown-menu">
							<li><a href="<?php echo base_url('webmaster/edit_kategori').'/'.$value2['id_kategori']; ?>"><span class="glyphicon glyphicon-pencil"></span> Edit</a></li>
							<li><a onClick="return confirm('Anda yakin ingin menghapus data ini ?');" href="<?php echo base_url('webmaster/hapus_kategori').'/'.$value2['id_kategori']; ?>"><span class="glyphicon glyphicon-trash"></span> Hapus</a></li>
						</ul>
					</div>
				</td>
					</tr>
				<?php } ?>
				
			</tr>
			<?php $no++; } ?>
		</tbody>
	</table>

	
