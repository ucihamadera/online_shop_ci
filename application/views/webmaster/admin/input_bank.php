<ol class="breadcrumb">
	<li>
		<a href="<?php echo base_url('webmaster'); ?>">Home</a>
	</li>
	<li>
		<a href="<?php echo base_url('webmaster/bank'); ?>">Bank</a>
	</li>
	<li class="active">Tambah Data</li>
</ol>


<?php echo form_open('webmaster/simpan_bank', 'role="form"'); ?>
<?php echo $error =  validation_errors() ? '<div class="alert alert-warning">'.validation_errors().'</div>':''; ?>
	<legend><span class="glyphicon glyphicon-user"></span> Tambah Data bank</legend>
	<input type="hidden" name="kode" value="<?php echo $kode_bank = isset($kode) ? $kode:''; ?>">
	<div class="form-group">
		<label for="">Nama Bank</label>
		<input type="text" class="form-control"  required name="nama_bank" placeholder="Masukan Nama Bank" value="<?php echo $nama_bank= isset($nama_bank) ? $nama_bank:''; ?>">
	</div>
	<div class="form-group">
		<label for="">No.Rekening</label>
		<input type="text" class="form-control"  required name="no_rekening" placeholder="Masukan No Rekening" value="<?php echo $no_rekening= isset($no_rekening) ? $no_rekening:''; ?>">
	</div>
	<div class="form-group">
		<label for="">Atas Nama</label>
		<input type="text" class="form-control"  required name="atas_nama" placeholder="Atas nama" value="<?php echo $atas_nama= isset($atas_nama) ? $atas_nama:''; ?>">
	</div>
	

	<input type="hidden" name="st" value="<?php echo $status = isset($st) ? $st:'tambah'; ?>">
	<button type="submit" class="btn btn-primary">Simpan Data</button>
	<a href="<?php echo base_url('webmaster/bank'); ?>" class="btn btn-danger">Kembali</a>
<?php echo form_close(); ?>