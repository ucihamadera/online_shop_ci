<legend><span class="glyphicon glyphicon-bookmark"></span> Pemberitahuan</legend>
<div class="panel panel-primary">
	<div class="panel-heading">Silakan klik link di bawah ini untuk melihat pemberitahuan..</div>
	<div class="panel-body bg-warning">
	  <?php 
	  	$testimoni = $this->db->get_where('toa_testimoni', array('status'=>'Pending'));
	  	echo $result = ($testimoni->num_rows() > 0 ) ? '<a href="'.base_url('webmaster/testimoni').'">Terdapat '.$testimoni->num_rows(). ' Testimoni yang menunggu untuk diaktifkan.</a><br>':'';
		$pembayaran = $this->db->get_where('toa_pembayaran', array('status'=>'PENDING'));
	  	echo $result = ($pembayaran->num_rows() > 0 ) ? '<a href="'.base_url('webmaster/pembayaranPending').'">Terdapat '.$pembayaran->num_rows(). ' transaksi pembayaran yang menunggu dikonfirmasi.</a><br>':'';
		$order = $this->db->get_where('toa_order', array('status'=>'Pending'));
	  	echo $result = ($order->num_rows() > 0 ) ? '<a href="'.base_url('webmaster/orderPending').'">Terdapat '.$order->num_rows(). ' Pesanan yang menunggu dikonfirmasi.</a><br>':'';
	  	$orderLunas = $this->db->get_where('toa_order', array('status'=>'Lunas'));
	  	echo $result = ($orderLunas->num_rows() > 0 ) ? '<a href="'.base_url('webmaster/orderLunas').'">Terdapat '.$orderLunas->num_rows(). ' Pesanan yang sudah lunas dan menunggu pengiriman Nomor Resi.</a><br>':'';
	  	if($testimoni->num_rows() == 0 && $pembayaran->num_rows() == 0 && $order->num_rows() == 0 && $orderLunas->num_rows == 0 ):
	  		echo 'Belum Ada Pemberitahuan';
	  	endif;
	   ?>
	</div>
</div>
<legend><span class="glyphicon glyphicon-bookmark"></span> Informasi Toko</legend>
<table class="table table-bordered table-striped">
	<tbody>
		<tr>
			<th>Jumlah Member</th>
			<td><?php echo $this->db->get('toa_member')->num_rows(); ?></td>
		</tr>
		<tr>
			<th>Jumlah Pesanan</th>
			<td><?php echo $this->db->get('toa_order')->num_rows(); ?></td>
		</tr>
		<tr>
			<th>Jumlah Artikel</th>
			<td><?php echo $this->db->get('toa_artikel')->num_rows(); ?></td>
		</tr>
		<tr>
			<th>Jumlah Testimoni</th>
			<td><?php echo $this->db->get('toa_testimoni')->num_rows(); ?></td>
		</tr>
		<tr>
			<th>Jumlah Kategori</th>
			<td><?php echo $this->db->get('toa_kategori')->num_rows(); ?></td>
		</tr>
	</tbody>
	
</table>