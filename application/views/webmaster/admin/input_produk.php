<ol class="breadcrumb">
	<li>
		<a href="<?php echo base_url('webmaster'); ?>">Home</a>
	</li>
	<li>
		<a href="<?php echo base_url('webmaster/produk'); ?>">Produk</a>
	</li>
	<li class="active">Tambah Data</li>
</ol>


<?php echo form_open_multipart('webmaster/simpan_produk', 'role="form"'); ?>
<?php echo $msg = $this->session->flashdata('result')?'<div class="alert alert-info">'.$this->session->flashdata('result').'</div>':''; ?>
<?php echo $error =  validation_errors() ? '<div class="alert alert-warning">'.validation_errors().'</div>':''; ?>
<?php echo $upload_error =  $this->upload->display_errors() ? '<div class="alert alert-warning">'.$this->upload->display_errors().'</div>':''; ?>
	<legend><span class="glyphicon glyphicon-flag"></span> Tambah Data Produk</legend>
	<input type="hidden" name="kode" value="<?php echo $kode_produk = isset($kode) ? $kode:''; ?>">
	<div class="form-group">
		<label for="">Nama Produk</label>
		<input type="text" class="form-control" name="nama" placeholder="Masukan Nama produk" value="<?php echo $nama= isset($nama) ? $nama:''; ?>">
	</div>
	<div class="form-group">
		<label for="">Kategori</label>
		<select name="kategori" class="form-control">
			<option value="">Pilih Kategori</option>
			<?php 
			foreach ($kategori->result_array() as $key => $value) {
				$id = isset($value['id_kategori'])?$value['id_kategori']:'';
				$nama = isset($value['nama_kategori'])?$value['nama_kategori']:'Belum Ada Kategori';
				$selected = ($id_kategori == $id)  ? "selected='selected'":'';
				echo "<option $selected value=" .$id. ">". $nama."</option>";
			}
				 ?>
		</select>
	</div>
	<div class="form-group">
		<label for="">Harga (Rp)</label>
		<input type="text" class="form-control" name="harga" placeholder="Masukan Harga produk" value="<?php echo $harga= isset($harga) ? $harga:''; ?>">
	</div>
	<div class="form-group">
		<label for="">Stok Barang</label>
		<input type="text" class="form-control" name="stok" placeholder="Masukan stok produk" value="<?php echo $stok= isset($stok) ? $stok:''; ?>">
	</div>
	<div class="form-group">
		<label for="">Berat (kg)</label>
		<input type="text" class="form-control" name="berat" placeholder="Masukan Berat produk" value="<?php echo $berat= isset($berat) ? $berat:''; ?>">
	</div>
	<div class="form-group">
		<label for="">Deskripsi produk</label>
		<textarea name="deskripsi"><?php echo $deskripsi= isset($deskripsi) ? $deskripsi:''; ?></textarea>
	</div>
	<?php if(isset($gambar)) { ?>
	<div class="form-group">
		<label for="">Preview</label>
		<img class="img-responsive" alt="gambar_produk" src="<?php echo base_url('asset/images/produk').'/'.$gambar=isset($gambar)?$gambar:''; ?>">
	</div>
	<?php } ?>
	<div class="form-group">
		<label for="">Upload Gambar</label>
		<input type="file" name="gb">
		<span class="help-block">Silakan pilih gambar.</span>
	</div>
	<input type="hidden" name="st" value="<?php echo $status = isset($st) ? $st:'tambah'; ?>">
	<button type="submit" class="btn btn-primary">Simpan Data</button>
	<a href="<?php echo base_url('webmaster/profil'); ?>" class="btn btn-danger">Kembali</a>
<?php echo form_close(); ?>
