<ol class="breadcrumb">
	<li>
		<a href="<?php echo base_url('webmaster'); ?>">Home</a>
	</li>
	<li>
		<a href="<?php echo base_url('webmaster/member'); ?>">Member</a>
	</li>
	<li class="active">Tambah Data</li>
</ol>


<?php echo form_open('webmaster/simpan_member', 'role="form"'); ?>
<?php echo $error =  validation_errors() ? '<div class="alert alert-warning">'.validation_errors().'</div>':''; ?>
	<legend><span class="glyphicon glyphicon-user"></span> Tambah Data member</legend>
	<input type="hidden" name="kode" value="<?php echo $kode_member = isset($kode) ? $kode:''; ?>">
	<div class="form-group">
		<label for="">Email</label>
		<input type="text" class="form-control"  required name="email" placeholder="Masukan Email" value="<?php echo $email_member= isset($email) ? $email:''; ?>">
	</div>
	<div class="form-group">
		<label for="">No.Telp</label>
		<input type="text" class="form-control"  required name="no_telp" placeholder="Masukan no_telp" value="<?php echo $no_telp= isset($no_telp) ? $no_telp:''; ?>">
	</div>
	<div class="form-group">
		<label for="">Password</label>
		<input type="text" class="form-control"  required name="password" placeholder="Masukan Password" value="<?php echo $pass= isset($password) ? $password:''; ?>">
	</div>
	<div class="form-group">
		<label for="">Status</label>
		<select required name="status" class="form-control">
		<?php 
		$data = array('0','1');
		$status= isset($status) ? $status:''; 
		foreach ($data as $key => $value) {
			$selected = ($value == $status) ? "selected='selected'":'';
			echo "<option value='$value' $selected >"; echo $value2 = ($value==1)?'Aktif':'Tidak Aktif'; echo "</option>";
		}
		?>
		</select>
	</div>

	<input type="hidden" name="st" value="<?php echo $status = isset($st) ? $st:'tambah'; ?>">
	<button type="submit" class="btn btn-primary">Simpan Data</button>
	<a href="<?php echo base_url('webmaster/member'); ?>" class="btn btn-danger">Kembali</a>
<?php echo form_close(); ?>