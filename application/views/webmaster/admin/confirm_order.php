<ol class="breadcrumb">
	<li>
		<a href="<?php echo base_url('webmaster'); ?>">Home</a>
	</li>
	<li>
		<a href="<?php echo base_url('webmaster/order'); ?>">Pesanan</a>
	</li>
	<li class="active">Tambah Data</li>
</ol>

<?php echo form_open('webmaster/simpan_konfirmasi', 'role="form"'); ?>
<?php echo $error =  validation_errors() ? '<div class="alert alert-warning">'.validation_errors().'</div>':''; ?>
<?php echo $msg = $this->session->flashdata('result')?'<div class="alert alert-info">'.$this->session->flashdata('result').'</div>':''; ?>

	<legend><span class="glyphicon glyphicon-shopping-cart"></span> Konfirmasi Pesanan</legend>
	<input type="hidden" name="kode" value="<?php echo $kode_order = isset($kode) ? $kode:''; ?>">
	<input type="hidden" name="email" value="<?php echo $email = isset($email) ? $email:''; ?>">
	<input type="hidden" name="alamat1" value="<?php echo $alamat1 = isset($alamat1) ? $alamat1:''; ?>">
	<input type="hidden" name="alamat2" value="<?php echo $alamat2 = isset($alamat2) ? $alamat2:''; ?>">
	<input type="hidden" name="sub_total" value="<?php echo $sub_total = isset($sub_total) ? $sub_total:''; ?>">
	<div class="form-group">
		<label for="">Alamat</label>
		<p class="text-danger"><?php echo $alamat2= isset($alamat2) ? strtoupper($alamat2):''; ?></p> 
	</div>
	<div class="form-group">
		<label for="">Nama Kurir</label>
		<input type="text" readonly class="form-control"  name="nama_kurir" value="<?php echo $nama_kurir= isset($nama_kurir) ? $nama_kurir:''; ?>">
	</div>
	<div class="form-group">
		<label for="">Berat Barang</label>
		<input type="text"  readonly class="form-control" name="berat" value="<?php echo $berat= isset($berat) ? $berat.' kg':''; ?>">
	</div>
	<div class="form-group">
		<label for=""><a target="_blank" href="http://cektarif.com/">Cek Ongkir</a></label>
		<input required class="form-control" type="text" name="ongkir" placeholder="Masukan Ongkir berdasarkan lokasi tujuan">
	</div>

	<button type="submit" class="btn btn-primary">Konfirmasi Pesanan</button>
	<a href="<?php echo base_url('webmaster/order'); ?>" class="btn btn-danger">Kembali</a>
<?php echo form_close(); ?>