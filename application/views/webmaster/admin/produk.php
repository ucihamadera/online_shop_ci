<ol class="breadcrumb">
	<li>
		<a href="<?php echo base_url('webmaster'); ?>">Beranda</a>
	</li>
	<li><a href="<?php echo base_url('webmaster/produk') ?>">Produk</a></li>
	<li><a id="tambah" href="<?php echo base_url('webmaster/tambah_produk');?>">Tambah Data</a></li>
</ol>
<div class="well text-right">
	   	<?php echo form_open('webmaster/cari_produk', 'class="form-inline" role="form"'); ?>
		<div class="form-group">
	   		<input type="text" class="form-control" required name="cari" placeholder="Cari Nama Produk ">	   
	   	</div>
	   	<button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-search"></span></button>
	   <?php echo form_close(); ?>
</div>	

<?php 
echo $msg = $this->session->flashdata('result')?'<div class="alert alert-info">'.$this->session->flashdata('result').'</div>':'';
?>

<table class="table table-bordered table-hover">
	<thead>
		<tr>
			<th>No</th>
			<th>Produk</th>
			<th>Preview</th>
			<th>Berat</th>
			<th>Stok</th>
			<th>Harga</th>
			<th>Aksi</th>
		</tr>
	</thead>
	<tbody>
		<?php 

		echo $info = ($produk->num_rows()) >0 ? '':'<tr><td colspan="7"><center>Belum Ada Data</center></td></tr>';

		$no = $tot+1;
		foreach ($produk->result_array() as $key => $value) {
			?>
			<tr>
				<td><?php echo $no; ?></td>
				<td><?php echo '<strong>'.$value['nama_produk'].'</strong>'; ?></td>
				<td> <img height="100px" src="<?php echo base_url('asset/images/produk').'/'.$value['gambar']; ?>" alt="gambar_produk"> </td>
				<td><?php echo $value['berat'].' kg'; ?></td>
				<td><?php echo $value['stok']; ?></td>
				<td><?php echo 'Rp '.number_format($value['harga'], 2); ?></td>
				<td>
					<div class="btn-group">
						<a class="btn dropdown-toggle btn-primary" data-toggle="dropdown" href="#">Pilihan <span class="caret"></span></a>
						<ul class="dropdown-menu">
							<li><a href="<?php echo base_url('webmaster/edit_produk').'/'.$value['kode_produk']; ?>"><span class="glyphicon glyphicon-pencil"></span> Edit</a></li>
							<li><a onClick="return confirm('Anda yakin ingin menghapus data ini ?');" href="<?php echo base_url('webmaster/hapus_produk').'/'.$value['kode_produk']; ?>"><span class="glyphicon glyphicon-trash"></span> Hapus</a></li>
						</ul>
					</div>
				</td>
			</tr>
			<?php $no++; } ?>
		</tbody>
	</table>
	<div class="pagination"><?php echo $paginator; ?></div>
	