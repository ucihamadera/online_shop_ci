<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Testimoni extends CI_Controller {

	public function index()
	{
		$data['judul'] = 'Semua Testimoni | ';
		$data['testimoni'] = $this->db->order_by('nama', 'ASC')->get_where('toa_testimoni', array('status'=>1));
	
		$data['kategori'] = $this->db->get_where('toa_kategori', array('kode_level'=>0, 'kode_parent'=>0));

		$this->load->view('web/header', $data);
		$this->load->view('web/sidebar',$data);
		$this->load->view('web/testimoni', $data);
		$this->load->view('web/footer');
	}

	public function validateTestimoni()
	{
		
			// set rules untuk proses validasi
			$this->form_validation->set_rules('nama', 'Nama', 'trim|required');
			$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
			$this->form_validation->set_rules('isi', 'Isi Testimoni', 'trim|required');
			return $this->form_validation->run();
	}
	public function simpan_testimoni()
	{
		if(!$this->input->is_ajax_request())
		{
			redirect('testimoni');
		}
		else
		{
			// cek validasi
			$result = $this->validateTestimoni();
			if(!$result)
			{
				$msg = array('success' => false,'isi' => validation_errors());
			}
			else
			{
				$data = array(
					"kode_testimoni" => $this->Webmodel->ambilKode('kode_testimoni', 'toa_testimoni', 'TEST'),
					"waktu" => date('Y-m-d_H:i:s'),
					"nama" => $this->input->post('nama', TRUE),
					"email" => $this->input->post('email', TRUE),
					"isi" => $this->input->post('isi', TRUE),
					"status" => 0
					);
				// cek apakah data testimoni ditemukan
				$cek = $this->Webmodel->simpan_testimoni('toa_testimoni', $data);
				if(!$cek)
				{
					// jika tidak ditemukan maka testimoni gagal
					$msg = array('success' => false,'isi' => "Testimoni gagal dikirim, Silakan Ulangi..");
				} 
				else
				{
					$msg = array('success' => true,'isi' => "Testimoni berhasil dikirim, isi testimoni akan ditampilkan setelah disetujui..");
					
				}
			}
			echo json_encode($msg);
		}
	}

}

/* End of file Testimoni.php */
/* Location: ./application/controllers/Testimoni.php */