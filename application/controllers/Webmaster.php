<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Webmaster extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
	}
	public function index()
	{
		if($this->session->userdata('isLoggedIn') == "admin") 
		{
			$this->load->view('webmaster/admin/header');
			$this->load->view('webmaster/admin/home');
			$this->load->view('webmaster/admin/footer');
		}
		else 
		{
			$data['title'] = 'Login Admin';
			$data['credit'] = 'Copyright &copy; 2015 | <a href="">Emade Haryo Kuncoro</a>';
			$this->load->view('webmaster/login',$data);
		}
	}
	public function validateLogin()
	{

		if($this->session->userdata('isLoggedIn') == "admin")
		{
			redirect('webmaster');
		}
		else
		{

			// set rules untuk proses validasi
			$this->form_validation->set_rules('username', 'Username', 'trim|required');
			$this->form_validation->set_rules('password', 'Password', 'trim|required');
			if($this->form_validation->run() == FALSE)
			{
				return FALSE;
			}
			else
			{
				return TRUE;
			}
		}
	}
	public function login()
	{
		if(!$this->input->is_ajax_request())
		{
			redirect('webmaster');
		}
		
		if($this->session->userdata('isLoggedIn') == "admin")
		{
			redirect('webmaster');
		}
		else
		{
			// cek validasi
			$result = $this->validateLogin();
			if(!$result)
			{
				$msg = array('success' => false,'isi' => validation_errors());
			}
			else
			{

				$data = array(
					"user_admin" => $this->input->post('username', TRUE),
					"pass_admin" => $this->input->post('password', TRUE),
					"status_admin" => 1
					);
				// cek apakah data login ditemukan
				$cek = $this->Adminmodel->cekLogin($data);
				if(!$cek)
				{
					// jika tidak ditemukan maka login gagal
					$msg = array('success' => false,'isi' => "Username atau Password yang anda masukan salah, Silakan Ulangi..");
				} 
				else
				{
					// jika return true maka login berhasil
					$msg = array('success' => true,'isi' => "Login sukses.. Anda akan dialihkan ke halaman Admin..");
				}
			}
			echo json_encode($msg);
		}
	}
	public function logout()
	{
		$this->session->sess_destroy();
		redirect('webmaster');
	}

	public function profil()
	{
		if($this->session->userdata('isLoggedIn') == "admin")
		{
			$page=$this->uri->segment(3);
			$limit=5;
			if(!$page):
				$offset = 0;
			else:
				$offset = $page;
			endif;
			
			$data["profil"] = $this->Adminmodel->lihat_tabel('toa_admin','nama_admin','ASC',$limit,$offset);
			
			$config['base_url'] = base_url() . 'webmaster/profil/';
			$config['total_rows'] = $this->Adminmodel->hitung_isi_tabel('toa_admin','');
			$config['per_page'] = $limit;
			$config['uri_segment'] = 3;
			$this->pagination->initialize($config);
			$data["paginator"] =$this->pagination->create_links();
			$data['tot'] = $offset;
			$this->load->view('webmaster/admin/header', $data);
			$this->load->view('webmaster/admin/profil', $data);
			$this->load->view('webmaster/admin/footer', $data);
		}
		else
		{
			redirect('webmaster');
		}
	}
	public function tambah_profil()
	{
		if($this->session->userdata('isLoggedIn') == "admin")
		{
			$this->load->view('webmaster/admin/header');
			$this->load->view('webmaster/admin/input_profil');
			$this->load->view('webmaster/admin/footer');
		}
		else
		{
			redirect('webmaster');
		}
		
	}

	public function edit_profil($kode="")
	{
		if($this->session->userdata('isLoggedIn') == "admin")
		{
			$id = isset($kode) ? $kode: $this->uri->segment(3);
			$kode_admin = array('kode_admin'=>$id);
			$q = $this->Adminmodel->ambilDataByID('toa_admin',$kode_admin);
			foreach ($q as $value) {
				$data['kode']  = $value->kode_admin; 
				$data['nama']  = $value->nama_admin; 
				$data['user_admin']  = $value->user_admin; 
			}
			$data['st'] = 'edit';

			$this->load->view('webmaster/admin/header');
			$this->load->view('webmaster/admin/input_profil',$data);
			$this->load->view('webmaster/admin/footer');
		}
		else
		{
			redirect('webmaster');
		}
	}

	public function validateProfil()
	{
		if($this->session->userdata('isLoggedIn') == "admin")
		{
			// set rules untuk proses validasi
			$this->form_validation->set_rules('nama', 'Nama Lengkap', 'trim|required|min_length[5]');
			$this->form_validation->set_rules('username', 'Username', 'trim|required|min_length[3]');
			$this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[8]');

			return $this->form_validation->run();			
		}
		else
		{
			redirect('webmaster');

		}
	}

	public function simpan_profil()
	{
		if($this->session->userdata('isLoggedIn') == "admin")
		{
			$cek_profil = $this->validateProfil();

			$st = $this->input->post('st', TRUE);
			$kode_admin = ($st=="tambah") ? $this->Adminmodel->ambilKode('kode_admin','toa_admin','ADM_'):$this->input->post('kode', TRUE);

			if(!$cek_profil)
			{
				return $hasil = ($st == "tambah") ? $this->tambah_profil():$this->edit_profil($kode=$kode_admin);
			}


			$user_admin = $this->input->post('username', TRUE);
			$salt = mcrypt_create_iv(22, MCRYPT_DEV_URANDOM);
			$options = [
    		'cost' => 12,
    		'salt' => $salt,
			];
			$pass_admin = password_hash($this->input->post('password', TRUE), PASSWORD_BCRYPT, $options);
			$nama_admin = $this->input->post('nama', TRUE);
			$status_admin = 1;
			

			$data = array('kode_admin'=>$kode_admin, 'user_admin'=>$user_admin, 'pass_admin'=>$pass_admin, 'nama_admin'=>$nama_admin, 'status_admin'=>$status_admin);

			if($st == 'tambah')
			{
				$this->Adminmodel->simpan_data('toa_admin', $data);
				redirect('webmaster/profil');
			}
			else if($st == 'edit')
			{
				$where = array('kode_admin'=>$kode_admin);
				$this->Adminmodel->update_data('toa_admin', $data, $where);
				redirect('webmaster/profil');
			}
			else 
			{
				redirect('webmaster');
			}
		}
		else
		{
			redirect('webmaster');
		}
	}

	public function hapus_profil($id)
	{
		if($this->session->userdata('isLoggedIn') == "admin")
		{
			$cek = $this->Adminmodel->hapus_tabel_byId('toa_admin','kode_admin',$id);
			redirect('webmaster/profil');
		}
		else
		{
			redirect('webmaster');
		}
		
	}
	// -----------------------------------------------------------------------------------------
	// Mulai fungsi member
	// -----------------------------------------------------------------------------------------
	public function member()
	{
		if($this->session->userdata('isLoggedIn') == "admin")
		{
			$page=$this->uri->segment(3);
			$limit=5;
			if(!$page):
				$offset = 0;
			else:
				$offset = $page;
			endif;
			
			$data["member"] = $this->Adminmodel->lihat_tabel('toa_member','email','ASC',$limit,$offset);
			
			$config['base_url'] = base_url() . 'webmaster/member/';
			$config['total_rows'] = $this->Adminmodel->hitung_isi_tabel('toa_member','');
			$config['per_page'] = $limit;
			$config['uri_segment'] = 3;
			$this->pagination->initialize($config);
			$data["paginator"] =$this->pagination->create_links();
			$data['tot'] = $offset;
			$this->load->view('webmaster/admin/header', $data);
			$this->load->view('webmaster/admin/member', $data);
			$this->load->view('webmaster/admin/footer', $data);
			
		}
		else
		{
			redirect('webmaster');
		}
			
	}

	public function cari_member()
	{

		if($this->session->userdata('isLoggedIn') == "admin")
		{
			if($this->input->post("cari")=="")
			{
				$kata = $this->session->userdata('kata');
			}
			else
			{
				$sess_data['kata'] = $this->input->post("cari");
				$this->session->set_userdata($sess_data);
				$kata = $this->session->userdata('kata');
			}
			
			$page=$this->uri->segment(3);
			$limit=5;
			if(!$page):
			$offset = 0;
			else:
			$offset = $page;
			endif;
			
			$d['tot'] = $offset;
			$config['base_url'] = base_url() . 'webmaster/cari_member/';
			$config['total_rows'] = $this->Adminmodel->hitung_isi_tabel_cari('toa_member', 'email', $kata);
			$config['per_page'] = $limit;
			$config['uri_segment'] = 3;
			$this->pagination->initialize($config);
			$d["paginator"] =$this->pagination->create_links();
			
			$where = array('email'=>$kata);
			$d['member'] = $this->Adminmodel->lihat_tabel_cari('toa_member','email', 'ASC', $limit, $offset, $where);
			$this->load->view('webmaster/admin/header');
			$this->load->view('webmaster/admin/member', $d);
			$this->load->view('webmaster/admin/footer');

			
		}
		else
		{
			redirect('webmaster');
		}
			
	}

	public function tambah_member()
	{
		if($this->session->userdata('isLoggedIn') == "admin")
		{
			$this->load->view('webmaster/admin/header');
			$this->load->view('webmaster/admin/input_member');
			$this->load->view('webmaster/admin/footer');
		}
		else
		{
			redirect('webmaster');
		}
		
	}

	public function edit_member($kode="")
	{
		if($this->session->userdata('isLoggedIn') == "admin")
		{
			$id = isset($kode) ? $kode: $this->uri->segment(3);
			$kode_member = array('kode_member'=>$id);
			$q = $this->Adminmodel->ambilDataByID('toa_member',$kode_member);
			foreach ($q as $value) {
				$data['kode']  = $value->kode_member; 
				$data['email']  = $value->email; 
				$data['password']  = $value->password; 
				$data['no_telp']  = $value->no_telp; 
				$data['status']  = $value->status; 
			}
			$data['st'] = 'edit';

			$this->load->view('webmaster/admin/header');
			$this->load->view('webmaster/admin/input_member',$data);
			$this->load->view('webmaster/admin/footer');
		}
		else
		{
			redirect('webmaster');
		}
	}

	public function validateMember()
	{
		if($this->session->userdata('isLoggedIn') == "admin")
		{
			// set rules untuk proses validasi
			$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
			$this->form_validation->set_rules('no_telp', 'No Telp', 'trim|required|min_length[10]');
			$this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[8]');
			$this->form_validation->set_rules('status', 'Status', 'trim|required');

			return $this->form_validation->run();			
		}
		else
		{
			redirect('webmaster');

		}
	}

	public function simpan_member()
	{
		if($this->session->userdata('isLoggedIn') == "admin")
		{
			$cek_member = $this->validateMember();

			$st = $this->input->post('st', TRUE);
			$kode_member = ($st=="tambah") ? $this->Adminmodel->ambilKode('kode_member','toa_member','MTO_'):$this->input->post('kode', TRUE);

			if(!$cek_member)
			{
				return $hasil = ($st == "tambah") ? $this->tambah_member():$this->edit_member($kode=$kode_member);
			}


			$email = $this->input->post('email', TRUE);
			$password = md5($this->input->post('password', TRUE).'_member_toa');
			$no_telp = $this->input->post('no_telp', TRUE);
			$status = $this->input->post('status', TRUE);
			

			$data = array('kode_member'=>$kode_member, 'email'=>$email, 'password'=>$password, 'no_telp'=>$no_telp, 'status'=>$status);

			if($st == 'tambah')
			{
				$this->Adminmodel->simpan_data('toa_member', $data);
				redirect('webmaster/member');
			}
			else if($st == 'edit')
			{
				$where = array('kode_member'=>$kode_member);
				$this->Adminmodel->update_data('toa_member', $data, $where);
				redirect('webmaster/member');
			}
			else 
			{
				redirect('webmaster');
			}
		}
		else
		{
			redirect('webmaster');
		}
	}

	public function hapus_member($id)
	{
		if($this->session->userdata('isLoggedIn') == "admin")
		{
			$cek = $this->Adminmodel->hapus_tabel_byId('toa_member','kode_member',$id);
			redirect('webmaster/member');
		}
		else
		{
			redirect('webmaster');
		}
		
	}
	// -----------------------------------------------------------------------------------------------------
	// Akhir fungsi member
	// -----------------------------------------------------------------------------------------------------


	// -----------------------------------------------------------------------------------------------------
	// Mulai fungsi info
	// -----------------------------------------------------------------------------------------------------

	public function info()
	{
		if($this->session->userdata('isLoggedIn') == "admin")
		{
			$q = $this->db->get('toa_info');
			foreach ($q->result() as $value) {
				$data['kode']  = $value->kode_info; 
				$data['deskripsi']  = $value->deskripsi; 
				$data['keywords']  = $value->keywords; 
				$data['nama']  = $value->nama_toko; 
				$data['no_telp']  = $value->no_telp; 
				$data['alamat']  = $value->alamat; 
				$data['email']  = $value->email; 
				$data['fb']  = $value->fb; 
				$data['twitter']  = $value->twitter; 
				$data['google']  = $value->google; 
				$data['logo']  = $value->logo; 
			}
			$data['st'] = 'edit';

			$this->load->view('webmaster/admin/header');
			$this->load->view('webmaster/admin/info',$data);
			$this->load->view('webmaster/admin/footer');
		}
		else
		{
			redirect('webmaster');
		}
		
	}

	public function validateInfo()
	{
		if($this->session->userdata('isLoggedIn') == "admin")
		{
			// set rules untuk proses validasi
			$this->form_validation->set_rules('nama', 'Nama Toko', 'trim|required|min_length[5]');
			$this->form_validation->set_rules('deskripsi', 'Deskripsi Toko', 'trim|required|min_length[10]');
			$this->form_validation->set_rules('keywords', 'Kata Kunci', 'trim|required|min_length[5]');
			$this->form_validation->set_rules('alamat', 'Alamat', 'trim|required|min_length[5]');
			$this->form_validation->set_rules('no_telp', 'No Telp', 'trim|required|min_length[10]');
			$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');

			return $this->form_validation->run();			
		}
		else
		{
			redirect('webmaster');

		}
	}

	public function update_info()
	{
		if($this->session->userdata('isLoggedIn') == "admin")
		{
			$cek_info = $this->validateInfo();

			if(!$cek_info)
			{
				return $this->info();
			}

			$kode = $this->input->post('kode', TRUE);
			$nama = $this->input->post('nama', TRUE);
			$deskripsi = $this->input->post('deskripsi', TRUE);
			$keywords = str_replace(' ', '_', $this->input->post('keywords', TRUE));
			$alamat = $this->input->post('alamat', TRUE);
			$no_telp = $this->input->post('no_telp', TRUE);
			$email = $this->input->post('email', TRUE);
			$fb = $this->input->post('fb', TRUE);
			$twitter =  $this->input->post('twitter', TRUE);
			$google = $this->input->post('google', TRUE);
			$logo = $this->input->post('logo', TRUE);
			$nama_logo = $this->input->post('nama_logo', TRUE);


			if(empty($_FILES['logo']['name']))
				{
					$data = array('kode_info'=>$kode, 'nama_toko'=>$nama, 'deskripsi'=>$deskripsi,'keywords'=>$keywords, 'alamat'=>$alamat, 'no_telp'=>$no_telp, 'email'=>$email, 'google'=>$google, 'fb'=>$fb, 'twitter'=>$twitter);
					$where = array('kode_info'=>$kode); 
					$this->Adminmodel->update_data('toa_info', $data, $where);
					redirect('webmaster/info');
				}
				else
				{
					$bersih=$_FILES['logo']['name'];
					$nm=str_replace(" ","_","$bersih");
					$config["file_name"]=$nm; //dengan eekstensi
					$nama_fl=$nm; //simpan nama ini k database
					$config['upload_path'] = './asset/images/home';
					$config['allowed_types'] = 'bmp|gif|jpg|jpeg|png';
					$config['max_size'] = '2000';
					$config['max_width'] = '200';
					$config['max_height'] = '200';	
					$config['overwrite'] = TRUE;	
					$this->upload->initialize($config);				
				
					if(!$this->upload->do_upload('logo'))
					{
						$this->info();
					}
					else 
					{
						
						$data = array('kode_info'=>$kode, 'nama_toko'=>$nama, 'deskripsi'=>$deskripsi,'keywords'=>$keywords, 'alamat'=>$alamat, 'no_telp'=>$no_telp, 'email'=>$email, 'google'=>$google, 'fb'=>$fb, 'twitter'=>$twitter, 'logo'=>$nama_fl);
						$where = array('kode_info'=>$kode); 
						$this->Adminmodel->update_data('toa_info',$data, $where);
						redirect('webmaster/info');
					}
				}


		}
		else
		{
			redirect('webmaster');
		}
	}

	// -------------------------------------------------------------------------------------------------------------
	// Akhir fungsi info
	// -------------------------------------------------------------------------------------------------------------

	// -------------------------------------------------------------------------------------------------------------
	// Awal fungsi banner
	// -------------------------------------------------------------------------------------------------------------
	public function banner()
	{
		if($this->session->userdata('isLoggedIn') == "admin")
		{
			$page=$this->uri->segment(3);
			$limit=5;
			if(!$page):
				$offset = 0;
			else:
				$offset = $page;
			endif;
			
			$data["banner"] = $this->Adminmodel->lihat_tabel('toa_banner','judul','ASC',$limit,$offset);
			
			$config['base_url'] = base_url() . 'webmaster/banner/';
			$config['total_rows'] = $this->Adminmodel->hitung_isi_tabel('toa_banner','');
			$config['per_page'] = $limit;
			$config['uri_segment'] = 3;
			$this->pagination->initialize($config);
			$data["paginator"] =$this->pagination->create_links();
			$data['tot'] = $offset;
			$this->load->view('webmaster/admin/header', $data);
			$this->load->view('webmaster/admin/banner', $data);
			$this->load->view('webmaster/admin/footer', $data);
			
		}
		else
		{
			redirect('webmaster');
		}
			
	}

	public function tambah_banner()
	{
		if($this->session->userdata('isLoggedIn') == "admin")
		{
			$data['status'] = 0;
			$this->load->view('webmaster/admin/header');
			$this->load->view('webmaster/admin/input_banner', $data);
			$this->load->view('webmaster/admin/footer');
		}
		else
		{
			redirect('webmaster');
		}
		
	}

	public function validateBanner()
	{
		if($this->session->userdata('isLoggedIn') == "admin")
		{
			// set rules untuk proses validasi
			$this->form_validation->set_rules('judul', 'Judul Banner', 'trim|required|min_length[10]');
			$this->form_validation->set_rules('deskripsi', 'Deskripsi Banner', 'required|min_length[10]');

			return $this->form_validation->run();			
		}
		else
		{
			redirect('webmaster');

		}
	}

	public function simpan_banner()
	{
		if($this->session->userdata('isLoggedIn') == "admin")
		{
			$cek_banner = $this->validateBanner();

			$st = $this->input->post('st', TRUE);
			$kode_banner = ($st=="tambah") ? $this->Adminmodel->ambilKode('kode_banner','toa_banner','BN_'):$this->input->post('kode', TRUE);

			if(!$cek_banner)
			{
				return $hasil = ($st == "tambah") ? $this->tambah_banner():$this->edit_banner($kode=$kode_banner);
			}


			$judul = $this->input->post('judul', TRUE);
			$deskripsi = $this->input->post('deskripsi', FALSE);
			$gb = $this->input->post('gb', TRUE);
			$status = $this->input->post('status', TRUE);


			$bersih=$_FILES['gb']['name'];
			$nm = str_replace(" ","_","$bersih");
			$config["file_name"]=$nm; //dengan eekstensi
			$nama_fl=$nm; //simpan nama ini k database
			$config['upload_path'] = './asset/images/home';
			$config['allowed_types'] = 'bmp|gif|jpg|jpeg|png';
			$config['max_size'] = '2000';
			$config['max_width'] = '500';
			$config['max_height'] = '500';	
			$config['overwrite'] = TRUE;	
			$this->upload->initialize($config);	
				
			
			

			if($st == 'tambah')
			{
				if(empty($_FILES['gb']['name']))
				{
					$this->session->set_flashdata('result', 'Anda belum memilih gambar banner.');
					return $this->tambah_banner();
				}
				else 
				{
					if(!$this->upload->do_upload('gb'))
					{
						$this->tambah_banner();
					}
					else
					{
						$data = array('kode_banner'=>$kode_banner, 'judul'=>$judul, 'deskripsi'=>$deskripsi, 'gambar'=>$nama_fl, 'status'=>$status);
						$this->Adminmodel->simpan_data('toa_banner',$data);
						redirect('webmaster/banner');
					}
				}

			}
			else if($st == 'edit')
			{
				if(empty($_FILES['gb']['name']))
				{
					$data = array('kode_banner'=>$kode_banner, 'judul'=>$judul, 'deskripsi'=>$deskripsi, 'status'=>$status);
					$where = array('kode_banner'=>$kode_banner); 
					$this->Adminmodel->update_data('toa_banner', $data, $where);
					redirect('webmaster/banner');
				}
				else 
				{
					if(!$this->upload->do_upload('gb'))
					{
						$this->edit_banner($kode=$kode_banner);
					}
					else
					{
						$data = array('kode_banner'=>$kode_banner, 'judul'=>$judul, 'deskripsi'=>$deskripsi, 'gambar'=>$nama_fl, 'status'=>$status);
						$where = array('kode_banner'=>$kode_banner); 
						$this->Adminmodel->update_data('toa_banner',$data, $where);
						redirect('webmaster/banner');
					}
				}


				
			}
			else 
			{
				redirect('webmaster');
			}
		}
		else
		{
			redirect('webmaster');
		}
	}

	public function edit_banner($kode="")
	{
		if($this->session->userdata('isLoggedIn') == "admin")
		{
			$id = isset($kode) ? $kode: $this->uri->segment(3);
			$kode_banner = array('kode_banner'=>$id);
			$q = $this->Adminmodel->ambilDataByID('toa_banner',$kode_banner);
			foreach ($q as $value) {
				$data['kode']  = $value->kode_banner; 
				$data['judul']  = $value->judul; 
				$data['deskripsi']  = $value->deskripsi; 
				$data['gambar']  = $value->gambar; 
				$data['status']  = $value->status; 
			}
			$data['st'] = 'edit';

			$this->load->view('webmaster/admin/header');
			$this->load->view('webmaster/admin/input_banner',$data);
			$this->load->view('webmaster/admin/footer');
		}
		else
		{
			redirect('webmaster');
		}
	}

	public function hapus_banner($id)
	{
		if($this->session->userdata('isLoggedIn') == "admin")
		{
			$cek = $this->Adminmodel->hapus_tabel_byId('toa_banner','kode_banner',$id);
			redirect('webmaster/banner');
		}
		else
		{
			redirect('webmaster');
		}
	}

	public function set_banner($id)
	{
		if($this->session->userdata('isLoggedIn') == "admin")
		{
			$this->Adminmodel->set_banner($id);
			redirect('webmaster/banner');
		}
		else
		{
			redirect('webmaster');
		}
	}

	// -------------------------------------------------------------------------------------------------------------
	// Akhir fungsi banner
	// -------------------------------------------------------------------------------------------------------------


	// -------------------------------------------------------------------------------------------------------------
	// Awal fungsi halaman
	// -------------------------------------------------------------------------------------------------------------
	public function cara_belanja()
	{
		if($this->session->userdata('isLoggedIn') == "admin")
		{
			$q = $this->db->get('toa_info');
			foreach ($q->result() as $value) {
				$data['kode']  = $value->kode_info; 
				$data['deskripsi']  = $value->cara_belanja; 
				
			}
			$data['st'] = 'edit';

			$this->load->view('webmaster/admin/header');
			$this->load->view('webmaster/admin/cara_belanja',$data);
			$this->load->view('webmaster/admin/footer');
		}
		else
		{
			redirect('webmaster');
		}
		
	}

	public function simpan_cara_belanja($id)
	{
		if($this->session->userdata('isLoggedIn') == "admin")
		{
			$kode = $this->input->post('kode', TRUE);
			$cara_belanja = $this->input->post('deskripsi', FALSE);
			$data = array('cara_belanja' => $cara_belanja );
			$where  = array('kode_info' => $kode );
			$this->Adminmodel->update_data('toa_info',$data,$where);
			redirect('webmaster/cara_belanja');
		}
		else
		{
			redirect('webmaster');
		}
		
	}

	public function tentang_kami()
	{
		if($this->session->userdata('isLoggedIn') == "admin")
		{
			$q = $this->db->get('toa_info');
			foreach ($q->result() as $value) {
				$data['kode']  = $value->kode_info; 
				$data['deskripsi']  = $value->tentang_kami; 
				
			}
			$data['st'] = 'edit';

			$this->load->view('webmaster/admin/header');
			$this->load->view('webmaster/admin/tentang_kami',$data);
			$this->load->view('webmaster/admin/footer');
		}
		else
		{
			redirect('webmaster');
		}
		
	}

	public function simpan_tentang_kami($id)
	{
		if($this->session->userdata('isLoggedIn') == "admin")
		{
			$kode = $this->input->post('kode', TRUE);
			$tentang_kami = $this->input->post('deskripsi', FALSE);
			$data = array('tentang_kami' => $tentang_kami );
			$where  = array('kode_info' => $kode );
			$this->Adminmodel->update_data('toa_info',$data,$where);
			redirect('webmaster/tentang_kami');
		}
		else
		{
			redirect('webmaster');
		}
		
	}

	public function kebijakan_privasi()
	{
		if($this->session->userdata('isLoggedIn') == "admin")
		{
			$q = $this->db->get('toa_info');
			foreach ($q->result() as $value) {
				$data['kode']  = $value->kode_info; 
				$data['deskripsi']  = $value->kebijakan_privasi; 
				
			}
			$data['st'] = 'edit';

			$this->load->view('webmaster/admin/header');
			$this->load->view('webmaster/admin/kebijakan_privasi',$data);
			$this->load->view('webmaster/admin/footer');
		}
		else
		{
			redirect('webmaster');
		}
		
	}

	public function simpan_kebijakan_privasi($id)
	{
		if($this->session->userdata('isLoggedIn') == "admin")
		{
			$kode = $this->input->post('kode', TRUE);
			$kebijakan_privasi = $this->input->post('deskripsi', FALSE);
			$data = array('kebijakan_privasi' => $kebijakan_privasi );
			$where  = array('kode_info' => $kode );
			$this->Adminmodel->update_data('toa_info',$data,$where);
			redirect('webmaster/kebijakan_privasi');
		}
		else
		{
			redirect('webmaster');
		}
		
	}

	public function retur_produk()
	{
		if($this->session->userdata('isLoggedIn') == "admin")
		{
			$q = $this->db->get('toa_info');
			foreach ($q->result() as $value) {
				$data['kode']  = $value->kode_info; 
				$data['deskripsi']  = $value->aturan_retur_produk; 
				
			}
			$data['st'] = 'edit';

			$this->load->view('webmaster/admin/header');
			$this->load->view('webmaster/admin/retur_produk',$data);
			$this->load->view('webmaster/admin/footer');
		}
		else
		{
			redirect('webmaster');
		}
		
	}

	public function simpan_retur_produk($id)
	{
		if($this->session->userdata('isLoggedIn') == "admin")
		{
			$kode = $this->input->post('kode', TRUE);
			$retur_produk = $this->input->post('deskripsi', FALSE);
			$data = array('aturan_retur_produk' => $retur_produk );
			$where  = array('kode_info' => $kode );
			$this->Adminmodel->update_data('toa_info',$data,$where);
			redirect('webmaster/retur_produk');
		}
		else
		{
			redirect('webmaster');
		}
		
	}


	// -------------------------------------------------------------------------------------------------------------
	// Akhir fungsi halaman
	// -------------------------------------------------------------------------------------------------------------


	// -------------------------------------------------------------------------------------------------------------
	// Awal fungsi Artikel
	// -------------------------------------------------------------------------------------------------------------
	public function artikel()
	{

		if($this->session->userdata('isLoggedIn') == "admin")
		{
			$page=$this->uri->segment(3);
			$limit=10;
			if(!$page):
				$offset = 0;
			else:
				$offset = $page;
			endif;
			
			$data["artikel"] = $this->Adminmodel->lihat_tabel('toa_artikel','tanggal','DESC',$limit,$offset);
			
			$config['base_url'] = base_url() . 'webmaster/artikel/';
			$config['total_rows'] = $this->Adminmodel->hitung_isi_tabel('toa_artikel','');
			$config['per_page'] = $limit;
			$config['uri_segment'] = 3;
			$this->pagination->initialize($config);
			$data["paginator"] =$this->pagination->create_links();
			$data['tot'] = $offset;
			$this->load->view('webmaster/admin/header');
			$this->load->view('webmaster/admin/artikel', $data);
			$this->load->view('webmaster/admin/footer');
			
		}
		else
		{
			redirect('webmaster');
		}
			
	}

	public function cari_artikel()
	{

		if($this->session->userdata('isLoggedIn') == "admin")
		{
			if($this->input->post("cari")=="")
			{
				$kata = $this->session->userdata('kata');
			}
			else
			{
				$sess_data['kata'] = $this->input->post("cari");
				$this->session->set_userdata($sess_data);
				$kata = $this->session->userdata('kata');
			}
			
			$page=$this->uri->segment(3);
			$limit=5;
			if(!$page):
			$offset = 0;
			else:
			$offset = $page;
			endif;
			
			$d['tot'] = $offset;
			$config['base_url'] = base_url() . 'webmaster/cari_artikel/';
			$config['total_rows'] = $this->Adminmodel->hitung_isi_tabel_cari('toa_artikel', 'judul', $kata);
			$config['per_page'] = $limit;
			$config['uri_segment'] = 3;
			$this->pagination->initialize($config);
			$d["paginator"] =$this->pagination->create_links();
			
			$where = array('judul'=>$kata);
			$d['artikel'] = $this->Adminmodel->lihat_tabel_cari('toa_artikel','tanggal', 'DESC', $limit, $offset, $where);
			$this->load->view('webmaster/admin/header');
			$this->load->view('webmaster/admin/artikel', $d);
			$this->load->view('webmaster/admin/footer');

			
		}
		else
		{
			redirect('webmaster');
		}
			
	}

	public function tambah_artikel()
	{
		if($this->session->userdata('isLoggedIn') == "admin")
		{
			$this->load->view('webmaster/admin/header');
			$this->load->view('webmaster/admin/input_artikel');
			$this->load->view('webmaster/admin/footer');
		}
		else
		{
			redirect('webmaster');
		}
		
	}

	public function validateArtikel()
	{
		if($this->session->userdata('isLoggedIn') == "admin")
		{
			// set rules untuk proses validasi
			$this->form_validation->set_rules('judul', 'Judul Artikel', 'trim|required|min_length[10]');
			$this->form_validation->set_rules('isi', 'Isi Artikel', 'required|min_length[10]');

			return $this->form_validation->run();			
		}
		else
		{
			redirect('webmaster');

		}
	}

	public function simpan_artikel()
	{
		if($this->session->userdata('isLoggedIn') == "admin")
		{
			$cek_artikel = $this->validateArtikel();

			$st = $this->input->post('st', TRUE);
			$kode_artikel = ($st=="tambah") ? $this->Adminmodel->ambilKode('kode_artikel','toa_artikel','4'):$this->input->post('kode', TRUE);

			if(!$cek_artikel)
			{
				return $hasil = ($st == "tambah") ? $this->tambah_artikel():$this->edit_artikel($kode=$kode_artikel);
			}


			$judul = htmlentities($this->input->post('judul', TRUE));
			$deskripsi = $this->input->post('isi', FALSE);
			$gb = $this->input->post('gb', TRUE);
			$tanggal = date('Y-m-d');
			$jam = date('H:i:s');
			$dibaca =1;


			$bersih=$_FILES['gb']['name'];
			$nm = str_replace(" ","_","$bersih");
			$config["file_name"]=$nm; //dengan eekstensi
			$nama_fl=$nm; //simpan nama ini k database
			$config['upload_path'] = './asset/images/artikel';
			$config['allowed_types'] = 'bmp|gif|jpg|jpeg|png';
			$config['max_size'] = '2000';
			$config['max_width'] = '900';
			$config['max_height'] = '400';	
			$config['overwrite'] = TRUE;	
			$this->upload->initialize($config);	
				

			if($st == 'tambah')
			{
				if(empty($_FILES['gb']['name']))
				{
					$this->session->set_flashdata('result', 'Anda belum memilih gambar artikel.');
					return $this->tambah_artikel();
				}
				else 
				{
					if(!$this->upload->do_upload('gb'))
					{
						$this->tambah_artikel();
					}
					else
					{
						$data = array('kode_artikel'=>$kode_artikel, 'judul'=>$judul, 'isi_artikel'=>$deskripsi, 'tanggal'=>$tanggal, 'jam'=>$jam, 'gambar'=>$nama_fl, 'dibaca'=>$dibaca);
						$this->Adminmodel->simpan_data('toa_artikel',$data);
						redirect('webmaster/artikel');
					}
				}

			}
			else if($st == 'edit')
			{
				if(empty($_FILES['gb']['name']))
				{
					$data = array('kode_artikel'=>$kode_artikel, 'judul'=>$judul, 'tanggal'=>$tanggal, 'jam'=>$jam, 'isi_artikel'=>$deskripsi, 'dibaca'=>$dibaca);
					$where = array('kode_artikel'=>$kode_artikel); 
					$this->Adminmodel->update_data('toa_artikel', $data, $where);
					redirect('webmaster/artikel');
				}
				else 
				{
					if(!$this->upload->do_upload('gb'))
					{
						$this->edit_artikel($kode=$kode_artikel);
					}
					else
					{
						$data = array('kode_artikel'=>$kode_artikel, 'judul'=>$judul, 'tanggal'=>$tanggal, 'jam'=>$jam, 'isi_artikel'=>$deskripsi, 'gambar'=>$nama_fl, 'dibaca'=>$dibaca);
						$where = array('kode_artikel'=>$kode_artikel); 
						$this->Adminmodel->update_data('toa_artikel',$data, $where);
						redirect('webmaster/artikel');
					}
				}


				
			}
			else 
			{
				redirect('webmaster');
			}
		}
		else
		{
			redirect('webmaster');
		}
	}

	public function edit_artikel($kode="")
	{
		if($this->session->userdata('isLoggedIn') == "admin")
		{
			$id = isset($kode) ? $kode: $this->uri->segment(3);
			$kode_artikel = array('kode_artikel'=>$id);
			$q = $this->Adminmodel->ambilDataByID('toa_artikel',$kode_artikel);
			foreach ($q as $value) {
				$data['kode']  = $value->kode_artikel; 
				$data['judul']  = $value->judul; 
				$data['isi']  = $value->isi_artikel; 
				$data['gambar']  = $value->gambar; 
			}
			$data['st'] = 'edit';

			$this->load->view('webmaster/admin/header');
			$this->load->view('webmaster/admin/input_artikel',$data);
			$this->load->view('webmaster/admin/footer');
		}
		else
		{
			redirect('webmaster');
		}
	}

	public function hapus_artikel($id)
	{
		if($this->session->userdata('isLoggedIn') == "admin")
		{
			$cek = $this->Adminmodel->hapus_tabel_byId('toa_artikel','kode_artikel',$id);
			redirect('webmaster/artikel');
		}
		else
		{
			redirect('webmaster');
		}
	}

	// -------------------------------------------------------------------------------------------------------------
	// Akhir fungsi Artikel
	// -------------------------------------------------------------------------------------------------------------


	// -------------------------------------------------------------------------------------------------------------
	// Awal fungsi Produk
	// -------------------------------------------------------------------------------------------------------------

	public function produk()
	{

		if($this->session->userdata('isLoggedIn') == "admin")
		{
			$page=$this->uri->segment(3);
			$limit=10;
			if(!$page):
				$offset = 0;
			else:
				$offset = $page;
			endif;
			
			$data["produk"] = $this->Adminmodel->lihat_tabel('toa_produk','tgl_insert','DESC',$limit,$offset);
			
			$config['base_url'] = base_url() . 'webmaster/produk/';
			$config['total_rows'] = $this->Adminmodel->hitung_isi_tabel('toa_produk','');
			$config['per_page'] = $limit;
			$config['uri_segment'] = 3;
			$this->pagination->initialize($config);
			$data["paginator"] =$this->pagination->create_links();
			$data['tot'] = $offset;
			$this->load->view('webmaster/admin/header');
			$this->load->view('webmaster/admin/produk', $data);
			$this->load->view('webmaster/admin/footer');
			
		}
		else
		{
			redirect('webmaster');
		}
			
	}

	public function cari_produk()
	{

		if($this->session->userdata('isLoggedIn') == "admin")
		{
			if($this->input->post("cari")=="")
			{
				$kata = $this->session->userdata('kata');
			}
			else
			{
				$sess_data['kata'] = $this->input->post("cari");
				$this->session->set_userdata($sess_data);
				$kata = $this->session->userdata('kata');
			}
			
			$page=$this->uri->segment(3);
			$limit=5;
			if(!$page):
			$offset = 0;
			else:
			$offset = $page;
			endif;
			
			$d['tot'] = $offset;
			$config['base_url'] = base_url() . 'webmaster/cari_produk/';
			$config['total_rows'] = $this->Adminmodel->hitung_isi_tabel_cari('toa_produk', 'nama_produk', $kata);
			$config['per_page'] = $limit;
			$config['uri_segment'] = 3;
			$this->pagination->initialize($config);
			$d["paginator"] =$this->pagination->create_links();
			
			$where = array('nama_produk'=>$kata);
			$d['produk'] = $this->Adminmodel->lihat_tabel_cari('toa_produk','tgl_insert', 'DESC', $limit, $offset, $where);
			$this->load->view('webmaster/admin/header');
			$this->load->view('webmaster/admin/produk', $d);
			$this->load->view('webmaster/admin/footer');

			
		}
		else
		{
			redirect('webmaster');
		}
			
	}

	public function tambah_produk()
	{
		if($this->session->userdata('isLoggedIn') == "admin")
		{
			$data['kategori'] = $this->db->get('toa_kategori');

			$this->load->view('webmaster/admin/header');
			$this->load->view('webmaster/admin/input_produk',$data);
			$this->load->view('webmaster/admin/footer');
		}
		else
		{
			redirect('webmaster');
		}
		
	}

	public function validateproduk()
	{
		if($this->session->userdata('isLoggedIn') == "admin")
		{
			// set rules untuk proses validasi
			$this->form_validation->set_rules('nama', 'Nama Produk', 'trim|required|min_length[4]');
			$this->form_validation->set_rules('kategori', 'Kategori produk', 'required');
			$this->form_validation->set_rules('harga', 'Harga produk', 'integer|required|min_length[3]');
			$this->form_validation->set_rules('stok', 'Stok Barang', 'integer|required|min_length[1]');
			$this->form_validation->set_rules('berat', 'Berat produk', 'required|min_length[1]');

			return $this->form_validation->run();			
		}
		else
		{
			redirect('webmaster');

		}
	}

	public function simpan_produk()
	{
		if($this->session->userdata('isLoggedIn') == "admin")
		{
			$cek_produk = $this->validateproduk();

			$st = $this->input->post('st', TRUE);
			$kode_produk = ($st=="tambah") ? $this->Adminmodel->ambilKode('kode_produk','toa_produk','2'):$this->input->post('kode', TRUE);

			if(!$cek_produk)
			{
				return $hasil = ($st == "tambah") ? $this->tambah_produk():$this->edit_produk($kode=$kode_produk);
			}

			$nama = $this->input->post('nama', TRUE);
			$kategori = $this->input->post('kategori', TRUE);
			$harga = $this->input->post('harga', TRUE);
			$stok = $this->input->post('stok', TRUE);
			$berat = $this->input->post('berat', TRUE);
			$deskripsi = $this->input->post('deskripsi', FALSE);
			$gb = $this->input->post('gb', TRUE);
			$tgl = date('Y-m-d');
			$dibeli = 0;


			$bersih=$_FILES['gb']['name'];
			$nm = str_replace(" ","_","$bersih");
			$config["file_name"]=$nm; //dengan eekstensi
			$nama_fl=$nm; //simpan nama ini k database
			$config['upload_path'] = './asset/images/produk';
			$config['allowed_types'] = 'bmp|gif|jpg|jpeg|png';
			$config['max_size'] = '2000';
			$config['max_width'] = '900';
			$config['max_height'] = '500';	
			$config['overwrite'] = TRUE;	
			$this->upload->initialize($config);	
				
			
			

			if($st == 'tambah')
			{
				if(empty($_FILES['gb']['name']))
				{
					$this->session->set_flashdata('result', 'Anda belum memilih gambar produk.');
					return $this->tambah_produk();
				}
				else 
				{
					if(!$this->upload->do_upload('gb'))
					{
						$this->tambah_produk();
					}
					else
					{
						$data = array('kode_produk'=>$kode_produk, 'tgl_insert'=>$tgl, 'id_kategori'=>$kategori, 'nama_produk'=>$nama, 'deskripsi'=>$deskripsi, 'harga'=>$harga, 'stok'=>$stok, 'dibeli'=>$dibeli, 'berat'=>$berat,'gambar'=>$nama_fl);
						$this->Adminmodel->simpan_data('toa_produk',$data);
						redirect('webmaster/produk');
					}
				}

			}
			else if($st == 'edit')
			{
				if(empty($_FILES['gb']['name']))
				{
					$data = array('kode_produk'=>$kode_produk, 'tgl_insert'=>$tgl, 'id_kategori'=>$kategori, 'nama_produk'=>$nama, 'deskripsi'=>$deskripsi, 'harga'=>$harga, 'stok'=>$stok, 'dibeli'=>$dibeli, 'berat'=>$berat);
					$where = array('kode_produk'=>$kode_produk); 
					$this->Adminmodel->update_data('toa_produk', $data, $where);
					redirect('webmaster/produk');
				}
				else 
				{
					if(!$this->upload->do_upload('gb'))
					{
						$this->edit_produk($kode=$kode_produk);
					}
					else
					{
						$data = array('kode_produk'=>$kode_produk, 'tgl_insert'=>$tgl, 'id_kategori'=>$kategori, 'nama_produk'=>$nama, 'deskripsi'=>$deskripsi, 'harga'=>$harga, 'stok'=>$stok, 'dibeli'=>$dibeli, 'berat'=>$berat,'gambar'=>$nama_fl);
						$where = array('kode_produk'=>$kode_produk); 
						$this->Adminmodel->update_data('toa_produk',$data, $where);
						redirect('webmaster/produk');
					}
				}


				
			}
			else 
			{
				redirect('webmaster');
			}
		}
		else
		{
			redirect('webmaster');
		}
	}

	public function edit_produk($kode="")
	{
		if($this->session->userdata('isLoggedIn') == "admin")
		{
			$id = isset($kode) ? $kode: $this->uri->segment(3);
			$kode_produk = array('kode_produk'=>$id);
			$q = $this->Adminmodel->ambilDataByID('toa_produk',$kode_produk);
			foreach ($q as $value) {
				$data['kode']  = $value->kode_produk; 
				$data['id_kategori']  = $value->id_kategori; 
				$data['nama']  = $value->nama_produk; 
				$data['deskripsi']  = $value->deskripsi; 
				$data['berat']  = $value->berat; 
				$data['stok']  = $value->stok; 
				$data['harga']  = $value->harga; 
				$data['gambar']  = $value->gambar; 
			}
			$data['kategori'] = $this->db->get('toa_kategori');
			$data['st'] = 'edit';

			$this->load->view('webmaster/admin/header');
			$this->load->view('webmaster/admin/input_produk',$data);
			$this->load->view('webmaster/admin/footer');
		}
		else
		{
			redirect('webmaster');
		}
	}

	public function hapus_produk($id)
	{
		if($this->session->userdata('isLoggedIn') == "admin")
		{
			$cek = $this->Adminmodel->hapus_tabel_byId('toa_produk','kode_produk',$id);
			redirect('webmaster/produk');
		}
		else
		{
			redirect('webmaster');
		}
	}

	// -------------------------------------------------------------------------------------------------------------
	// Akhir fungsi Produk
	// -------------------------------------------------------------------------------------------------------------

	// -----------------------------------------------------------------------------------------
	// Mulai fungsi kategori
	// -----------------------------------------------------------------------------------------
	public function kategori()
	{
		if($this->session->userdata('isLoggedIn') == "admin")
		{
			
			$data["kategori"] = $this->db->get_where('toa_kategori',array('kode_level'=>0,'kode_parent'=>0));
			
			$this->load->view('webmaster/admin/header');
			$this->load->view('webmaster/admin/kategori', $data);
			$this->load->view('webmaster/admin/footer');
			
		}
		else
		{
			redirect('webmaster');
		}
			
	}

	public function tambah_kategori()
	{
		if($this->session->userdata('isLoggedIn') == "admin")
		{
			$data["kategori"] = $this->db->get_where('toa_kategori',array('kode_level'=>0,'kode_parent'=>0));

			$this->load->view('webmaster/admin/header');
			$this->load->view('webmaster/admin/input_kategori',$data);
			$this->load->view('webmaster/admin/footer');
		}
		else
		{
			redirect('webmaster');
		}
		
	}

	public function edit_kategori($kode="")
	{
		if($this->session->userdata('isLoggedIn') == "admin")
		{
			$id = isset($kode) ? $kode: $this->uri->segment(3);
			$kode_kategori = array('id_kategori'=>$id);
			$q = $this->Adminmodel->ambilDataByID('toa_kategori',$kode_kategori);
			foreach ($q as $value) {
				$data['kode']  = $value->id_kategori; 
				$data['nama']  = $value->nama_kategori; 
				$data['level']  = $value->kode_level; 
				$data['parent']  = $value->kode_parent; 
			}
			$data['st'] = 'edit';
			$data["kategori"] = $this->db->get_where('toa_kategori',array('kode_level'=>0,'kode_parent'=>0));

			$this->load->view('webmaster/admin/header');
			$this->load->view('webmaster/admin/input_kategori',$data);
			$this->load->view('webmaster/admin/footer');
		}
		else
		{
			redirect('webmaster');
		}
	}

	public function validateKategori()
	{
		if($this->session->userdata('isLoggedIn') == "admin")
		{
			// set rules untuk proses validasi
			$this->form_validation->set_rules('nama_kategori', 'Nama Kategori', 'trim|required');

			return $this->form_validation->run();			
		}
		else
		{
			redirect('webmaster');

		}
	}

	public function simpan_kategori()
	{
		if($this->session->userdata('isLoggedIn') == "admin")
		{
			$cek_kategori = $this->validateKategori();

			$st = $this->input->post('st', TRUE);
			$kode_kategori = ($st=="tambah") ? $this->Adminmodel->ambilKode('id_kategori','toa_kategori','KAT_'):$this->input->post('kode', TRUE);

			if(!$cek_kategori)
			{
				return $hasil = ($st == "tambah") ? $this->tambah_kategori():$this->edit_kategori($kode=$kode_kategori);
			}

			$parent = $this->input->post('kode_parent', TRUE);
			$nama = ucwords($this->input->post('nama_kategori', TRUE));
			$level = ($parent==0)?$level=0:$level=1;
			

			$data = array('id_kategori'=>$kode_kategori, 'nama_kategori'=>$nama, 'kode_level'=>$level, 'kode_parent'=>$parent);

			if($st == 'tambah')
			{
				$this->Adminmodel->simpan_data('toa_kategori', $data);
				redirect('webmaster/kategori');
			}
			else if($st == 'edit')
			{
				$where = array('id_kategori'=>$kode_kategori);
				$this->Adminmodel->update_data('toa_kategori', $data, $where);
				redirect('webmaster/kategori');
			}
			else 
			{
				redirect('webmaster');
			}
		}
		else
		{
			redirect('webmaster');
		}
	}

	public function hapus_kategori($id)
	{
		if($this->session->userdata('isLoggedIn') == "admin")
		{
			$cek = $this->Adminmodel->hapus_tabel_byId('toa_kategori','id_kategori',$id);
			redirect('webmaster/kategori');
		}
		else
		{
			redirect('webmaster');
		}
		
	}
	// -----------------------------------------------------------------------------------------------------
	// Akhir fungsi kategori
	// -----------------------------------------------------------------------------------------------------

	// -----------------------------------------------------------------------------------------------------
	// Awal fungsi order
	// -----------------------------------------------------------------------------------------------------

	public function order()
	{
		if($this->session->userdata('isLoggedIn') == "admin")
		{
			$page=$this->uri->segment(3);
			$limit=10;
			if(!$page):
				$offset = 0;
			else:
				$offset = $page;
			endif;
			
			$data["order"] = $this->Adminmodel->lihatPesanan($limit, $offset);
			
			$config['base_url'] = base_url() . 'webmaster/order/';
			$config['total_rows'] = $this->Adminmodel->JumlahlihatPesanan();
			$config['per_page'] = $limit;
			$config['uri_segment'] = 3;
			$this->pagination->initialize($config);
			$data["paginator"] =$this->pagination->create_links();
			$data['tot'] = $offset;
			$this->load->view('webmaster/admin/header', $data);
			$this->load->view('webmaster/admin/order', $data);
			$this->load->view('webmaster/admin/footer', $data);
			
		}
		else
		{
			redirect('webmaster');
		}
			
	}

	public function orderPending()
	{
		if($this->session->userdata('isLoggedIn') == "admin")
		{
			$page=$this->uri->segment(3);
			$limit=10;
			if(!$page):
				$offset = 0;
			else:
				$offset = $page;
			endif;
			
			$data["order"] = $this->Adminmodel->lihatPesananByStatus('Pending', $limit, $offset);
			
			$config['base_url'] = base_url() . 'webmaster/orderPending/';
			$config['total_rows'] = $this->Adminmodel->JumlahPesananByStatus('Pending');
			$config['per_page'] = $limit;
			$config['uri_segment'] = 3;
			$this->pagination->initialize($config);
			$data["paginator"] =$this->pagination->create_links();
			$data['tot'] = $offset;
			$this->load->view('webmaster/admin/header', $data);
			$this->load->view('webmaster/admin/order', $data);
			$this->load->view('webmaster/admin/footer', $data);
			
		}
		else
		{
			redirect('webmaster');
		}
			
	}

	public function orderBelumBayar()
	{
		if($this->session->userdata('isLoggedIn') == "admin")
		{
			$page=$this->uri->segment(3);
			$limit=10;
			if(!$page):
				$offset = 0;
			else:
				$offset = $page;
			endif;
			
			$data["order"] = $this->Adminmodel->lihatPesananByStatus('Belum Bayar', $limit, $offset);
			
			$config['base_url'] = base_url() . 'webmaster/orderBelumBayar/';
			$config['total_rows'] = $this->Adminmodel->JumlahPesananByStatus('Belum Bayar');
			$config['per_page'] = $limit;
			$config['uri_segment'] = 3;
			$this->pagination->initialize($config);
			$data["paginator"] =$this->pagination->create_links();
			$data['tot'] = $offset;
			$this->load->view('webmaster/admin/header', $data);
			$this->load->view('webmaster/admin/order', $data);
			$this->load->view('webmaster/admin/footer', $data);
			
		}
		else
		{
			redirect('webmaster');
		}
			
	}

	public function orderLunas()
	{
		if($this->session->userdata('isLoggedIn') == "admin")
		{
			$page=$this->uri->segment(3);
			$limit=10;
			if(!$page):
				$offset = 0;
			else:
				$offset = $page;
			endif;
			
			$data["order"] = $this->Adminmodel->lihatPesananByStatus('Lunas', $limit, $offset);
			
			$config['base_url'] = base_url() . 'webmaster/orderLunas/';
			$config['total_rows'] = $this->Adminmodel->JumlahPesananByStatus('Lunas');
			$config['per_page'] = $limit;
			$config['uri_segment'] = 3;
			$this->pagination->initialize($config);
			$data["paginator"] =$this->pagination->create_links();
			$data['tot'] = $offset;
			$this->load->view('webmaster/admin/header', $data);
			$this->load->view('webmaster/admin/order', $data);
			$this->load->view('webmaster/admin/footer', $data);
			
		}
		else
		{
			redirect('webmaster');
		}
			
	}

	public function orderTerkirim()
	{
		if($this->session->userdata('isLoggedIn') == "admin")
		{
			$page=$this->uri->segment(3);
			$limit=10;
			if(!$page):
				$offset = 0;
			else:
				$offset = $page;
			endif;
			
			$data["order"] = $this->Adminmodel->lihatPesananByStatus('Terkirim', $limit, $offset);
			
			$config['base_url'] = base_url() . 'webmaster/orderTerkirim/';
			$config['total_rows'] = $this->Adminmodel->JumlahPesananByStatus('Terkirim');
			$config['per_page'] = $limit;
			$config['uri_segment'] = 3;
			$this->pagination->initialize($config);
			$data["paginator"] =$this->pagination->create_links();
			$data['tot'] = $offset;
			$this->load->view('webmaster/admin/header', $data);
			$this->load->view('webmaster/admin/order', $data);
			$this->load->view('webmaster/admin/footer', $data);
			
		}
		else
		{
			redirect('webmaster');
		}
			
	}

	public function detail_order($id)
	{
		if($this->session->userdata('isLoggedIn') == "admin") 
		{
			$data['detail1'] = $this->db->get_where('toa_order', array('kode_order'=>$id));
			$data['detail2'] = $this->db->get_where('toa_detail_order', array('kode_order'=>$id));

			$this->load->view('webmaster/admin/header');
			$this->load->view('webmaster/admin/detail_order', $data);
			$this->load->view('webmaster/admin/footer');
		}
		else
		{
			redirect('user/daftar');
		}
	}

	public function konfirmasi_order($id)
	{
		if($this->session->userdata('isLoggedIn') == "admin")
		{
			$detail = $this->Adminmodel->lihatKonfirmasi($id);
			foreach ($detail->result_array() as $value) {
				$data['kode'] = $value['kode_order'];
				$data['alamat1'] = $value['alamat1'];
				$data['alamat2'] = $value['alamat2'];
				$data['nama_kurir'] = $value['nama_kurir'];
				$data['sub_total'] = $value['sub_total'];
				$data['email'] = $value['email'];

			}

			$data['berat'] = $this->Adminmodel->hitungBeratBarang($id);

			$this->load->view('webmaster/admin/header', $data);
			$this->load->view('webmaster/admin/confirm_order', $data);
			$this->load->view('webmaster/admin/footer', $data);
			
		}
		else
		{
			redirect('webmaster');
		}
			
	}

	public function input_resi($id)
	{
		if($this->session->userdata('isLoggedIn') == "admin")
		{
			$detail = $this->Adminmodel->lihatKonfirmasi($id);
			foreach ($detail->result_array() as $value) {
				$data['kode'] = $value['kode_order'];
				$data['alamat1'] = $value['alamat1'];
				$data['alamat2'] = $value['alamat2'];
				$data['nama_kurir'] = $value['nama_kurir'];
				$data['sub_total'] = $value['sub_total'];
				$data['ongkir'] = $value['ongkir'];
				$data['total_bayar'] = $value['total_bayar'];
				$data['email'] = $value['email'];

			}

			$data['berat'] = $this->Adminmodel->hitungBeratBarang($id);

			$this->load->view('webmaster/admin/header', $data);
			$this->load->view('webmaster/admin/input_resi', $data);
			$this->load->view('webmaster/admin/footer', $data);
			
		}
		else
		{
			redirect('webmaster');
		}
			
	}

	public function simpan_resi()
	{
		if($this->session->userdata('isLoggedIn') != "admin")
		{
			redirect('webmaster');
		}
		$email = $this->input->post('email', TRUE);
		$id = $this->input->post('kode', TRUE);
		$data['alamat1'] = $this->input->post('alamat1', TRUE);
		$data['alamat2'] = $this->input->post('alamat2', TRUE);
		$data['ongkir'] = $this->input->post('ongkir', TRUE);
		$data['sub_total'] = $this->input->post('sub_total', TRUE);
		$data['total_bayar'] = $this->input->post('total_bayar', TRUE); 

		//data pengiriman
		$kirim['kode_kirim'] = $this->Adminmodel->ambilKode('kode_kirim', 'toa_pengiriman', 'PG');
		$kirim['kode_order'] = $id;
		$kirim['tgl_kirim'] = $this->input->post('tgl_kirim', TRUE);
		$kirim['kurir'] = $this->input->post('nama_kurir', TRUE);
		$kirim['no_resi'] = $this->input->post('no_resi', TRUE);
		//data mail
		$tgl_kirim = $this->Adminmodel->ubahTanggal($kirim['tgl_kirim']);


		$this->Adminmodel->update_data('toa_order',$data,array('kode_order'=>$id));
		$link = base_url('user/konfirmasi_pembayaran');
		$message = "
		<table style='width:100%;border-collapse:collapse;border:3px solid #F9F9F9;box-shadow:0 0 7px #FE980F;background:#FE980F;padding:10px;font-size:18px;font-family:simplex;color:#FFF;'>
		<thead style='background:#FCF8E3;color:#FE980F;border-radius:5px;padding:10px;margin:10px;'>
		<tr>
		<th colspan='2'><h3>Pesanan Anda Sudah Kami Kirim</h3></th>
		</tr>
		<tr>
		<th colspan='2'>Detail Pengiriman dan Pembayaran.</th>
		</tr>
		</thead>
		<tbody style='background:#FCF8E3;color:#FE980F;border-radius:5px;padding:10px;margin:10px;'>
		<tr>
		<td colspan='2'>" .$data['alamat1']."<br>".$data['alamat2']. "</td>
		</tr>
		<tr>
		<td>Sub Total</td>
		<td>: Rp ".number_format($data['sub_total'], 2)."</td>
		</tr>
		<tr>
		<td>Ongkir</td>
		<td>: Rp ".number_format($data['ongkir'], 2)."</td>
		</tr>
		<tr>
		<td><b>Total Pembayaran</b></td>
		<td><b>: Rp ".number_format($data['total_bayar'], 2)."</b></td>
		</tr>
		<tr>
		<td><b>Tgl Kirim</b></td>
		<td><b>: ".$tgl_kirim."</b></td>
		</tr>
		<tr>
		<td><b>Jasa Pengiriman</b></td>
		<td><b>: ".$kirim['kurir']."</b></td>
		</tr>
		<tr>
		<td><b>No. Resi</b></td>
		<td><b>: ".$kirim['no_resi']."</b></td>
		</tr>
		<tr>
		<td colspan='2'>Terima kasih atas kepercayaan anda. Kami nantikan pesanan selanjutnya :) </td>
		</tr>
		<tr>
		<td colspan='2'>.</td>
		</tr>
		</tbody>
		</table>
		";
		$this->load->library('email');
		$this->email->set_newline("\r\n");
		$this->email->from('ehaku@momandkidsgoods.com','Admin MomAndKidsGoods[dot]com');
		$this->email->to($email); 
		$this->email->subject('Pesanan Sudah Dikirim');
		$this->email->message($message);
		if($this->email->send())
		{
			$this->Adminmodel->simpan_data('toa_pengiriman', $kirim);
			$this->Adminmodel->update_data('toa_order', array('status'=>'Terkirim') , array('kode_order'=>$id));
			$this->session->set_flashdata('result', 'Email Konfirmasi berhasil dikirim..');
			redirect('webmaster/order');
		}
		else
		{
			$this->session->set_flashdata('result', 'Email Konfirmasi Gagal dikirim, Silakan Ulangi');
			$this->input_resi($id);
		}

	}

	public function simpan_konfirmasi()
	{
		if($this->session->userdata('isLoggedIn') != "admin")
		{
			redirect('webmaster');
		}
		$email = $this->input->post('email', TRUE);
		$id = $this->input->post('kode', TRUE);
		$data['alamat1'] = $this->input->post('alamat1', TRUE);
		$data['alamat2'] = $this->input->post('alamat2', TRUE);
		$data['ongkir'] = $this->input->post('ongkir', TRUE);
		$data['sub_total'] = $this->input->post('sub_total', TRUE);
		$data['total_bayar'] = $data['ongkir']+$data['sub_total'] ;
		$data['status'] = 'Belum Bayar';

		$link = base_url('user/konfirmasi_pembayaran');
		$message = 
		"
		<table style='width:100%;border-collapse:collapse;border:3px solid #F9F9F9;box-shadow:0 0 7px #FE980F;background:#FE980F;padding:10px;font-size:18px;font-family:simplex;color:#FFF;'>
		<thead style='background:#FCF8E3;color:#FE980F;border-radius:5px;padding:10px;margin:10px;'>
		<tr>
		<th colspan='2'>Pesanan Anda Sudah Kami Terima</th>
		</tr>
		<tr>
		<th colspan='2'>Silakan lakukan pembayaran ke rekening bank yang telah anda pilih pada saat melakukan pemesanan barang.</th>
		</tr>
		<tr>
		<th colspan='2'>Detail Pengiriman dan Pembayaran.</th>
		</tr>
		</thead>
		<tbody style='background:#FCF8E3;color:#FE980F;border-radius:5px;padding:10px;margin:10px;'>
		<tr>
		<td colspan='2'>" .$data['alamat1']."<br>".$data['alamat2']. "</td>
		</tr>
		<tr>
		<td>Sub Total</td>
		<td>: Rp ".number_format($data['sub_total'], 2)."</td>
		</tr>
		<tr>
		<td>Ongkir</td>
		<td>: Rp ".number_format($data['ongkir'], 2)."</td>
		</tr>
		<tr>
		<td><b>Total Pembayaran</b></td>
		<td><b>: Rp ".number_format($data['total_bayar'], 2)."</b></td>
		</tr>
		<tr>
		<td colspan='2'>Setelah melakukan pembayaran, Silakan anda konfirmasi pembayaran anda ke link di bawah ini.</td>
		</tr>
		<tr>
		<td colspan='2'>.</td>
		</tr>
		<tr>
		<td colspan='2'>
		<a style='text-decoration:none;border-radius:3px;border:2px solid #fff; color:#fff;padding:5px; background:#FE980F;' href='".$link."'>Konfirmasi Pembayaran</a>
		</td>
		</tr>
		<tr>
		<td colspan='2'>.</td>
		</tr>
		</tbody>
		</table>
		";
		$this->load->library('email');
		$this->email->set_newline("\r\n");
		$this->email->from('webmaster@bikinaplikasiweb.com','Admin MomAndKidsGoods[dot]com');
		$this->email->to($email); 
		$this->email->subject('Pesanan Sudah Diterima');
		$this->email->message($message);
		if($this->email->send())
		{
			$this->Adminmodel->update_data('toa_order',$data,array('kode_order'=>$id));
			$this->session->set_flashdata('result', 'Email Konfirmasi berhasil dikirim..');
			redirect('webmaster/order');
		}
		else
		{
			$this->session->set_flashdata('result', 'Email Konfirmasi Gagal dikirim, Silakan Ulangi..');
			$this->konfirmasi_order($id);
		}

	}

	public function hapus_order($id)
	{
		if($this->session->userdata('isLoggedIn') == "admin")
		{
			$cek = $this->Adminmodel->hapus_tabel_byId('toa_order','kode_order',$id);
			redirect('webmaster/order');
		}
		else
		{
			redirect('webmaster');
		}
		
	}
	// -----------------------------------------------------------------------------------------------------
	// Akhir fungsi order
	// -----------------------------------------------------------------------------------------------------

	// -----------------------------------------------------------------------------------------
	// Mulai fungsi bank
	// -----------------------------------------------------------------------------------------
	public function bank()
	{
		if($this->session->userdata('isLoggedIn') == "admin")
		{
			$page=$this->uri->segment(3);
			$limit=5;
			if(!$page):
				$offset = 0;
			else:
				$offset = $page;
			endif;
			
			$data["bank"] = $this->Adminmodel->lihat_tabel('toa_bank','nama_bank','ASC',$limit,$offset);
			
			$config['base_url'] = base_url() . 'webmaster/bank/';
			$config['total_rows'] = $this->Adminmodel->hitung_isi_tabel('toa_bank','');
			$config['per_page'] = $limit;
			$config['uri_segment'] = 3;
			$this->pagination->initialize($config);
			$data["paginator"] =$this->pagination->create_links();
			$data['tot'] = $offset;
			$this->load->view('webmaster/admin/header', $data);
			$this->load->view('webmaster/admin/bank', $data);
			$this->load->view('webmaster/admin/footer', $data);
			
		}
		else
		{
			redirect('webmaster');
		}
			
	}

	public function tambah_bank()
	{
		if($this->session->userdata('isLoggedIn') == "admin")
		{
			$this->load->view('webmaster/admin/header');
			$this->load->view('webmaster/admin/input_bank');
			$this->load->view('webmaster/admin/footer');
		}
		else
		{
			redirect('webmaster');
		}
		
	}

	public function edit_bank($kode="")
	{
		if($this->session->userdata('isLoggedIn') == "admin")
		{
			$id = isset($kode) ? $kode: $this->uri->segment(3);
			$kode_bank = array('kode_bank'=>$id);
			$q = $this->Adminmodel->ambilDataByID('toa_bank',$kode_bank);
			foreach ($q as $value) {
				$data['kode']  = $value->kode_bank; 
				$data['nama_bank']  = $value->nama_bank; 
				$data['no_rekening']  = $value->no_rekening; 
				$data['atas_nama']  = $value->atas_nama; 
			}
			$data['st'] = 'edit';

			$this->load->view('webmaster/admin/header');
			$this->load->view('webmaster/admin/input_bank',$data);
			$this->load->view('webmaster/admin/footer');
		}
		else
		{
			redirect('webmaster');
		}
	}

	public function validatebank()
	{
		if($this->session->userdata('isLoggedIn') == "admin")
		{
			// set rules untuk proses validasi
			$this->form_validation->set_rules('nama_bank', 'Nama Bank', 'trim|required');
			$this->form_validation->set_rules('no_rekening', 'No Rekening', 'trim|required');
			$this->form_validation->set_rules('atas_nama', 'Atas Nama', 'trim|required');

			return $this->form_validation->run();			
		}
		else
		{
			redirect('webmaster');

		}
	}

	public function simpan_bank()
	{
		if($this->session->userdata('isLoggedIn') == "admin")
		{
			$cek_bank = $this->validatebank();

			$st = $this->input->post('st', TRUE);
			$kode_bank = ($st=="tambah") ? $this->Adminmodel->ambilKode('kode_bank','toa_bank','BK'):$this->input->post('kode', TRUE);

			if(!$cek_bank)
			{
				return $hasil = ($st == "tambah") ? $this->tambah_bank():$this->edit_bank($kode=$kode_bank);
			}


			$nama_bank = $this->input->post('nama_bank', TRUE);
			$no_rekening = $this->input->post('no_rekening', TRUE);
			$atas_nama = $this->input->post('atas_nama', TRUE);
			

			$data = array('kode_bank'=>$kode_bank, 'nama_bank'=>$nama_bank, 'no_rekening'=>$no_rekening, 'atas_nama'=>$atas_nama);

			if($st == 'tambah')
			{
				$this->Adminmodel->simpan_data('toa_bank', $data);
				redirect('webmaster/bank');
			}
			else if($st == 'edit')
			{
				$where = array('kode_bank'=>$kode_bank);
				$this->Adminmodel->update_data('toa_bank', $data, $where);
				redirect('webmaster/bank');
			}
			else 
			{
				redirect('webmaster');
			}
		}
		else
		{
			redirect('webmaster');
		}
	}

	public function hapus_bank($id)
	{
		if($this->session->userdata('isLoggedIn') == "admin")
		{
			$cek = $this->Adminmodel->hapus_tabel_byId('toa_bank','kode_bank',$id);
			redirect('webmaster/bank');
		}
		else
		{
			redirect('webmaster');
		}
		
	}
	// -----------------------------------------------------------------------------------------------------
	// Akhir fungsi bank
	// -----------------------------------------------------------------------------------------------------

	// -----------------------------------------------------------------------------------------
	// Mulai fungsi kurir
	// -----------------------------------------------------------------------------------------
	public function kurir()
	{
		if($this->session->userdata('isLoggedIn') == "admin")
		{
			$page=$this->uri->segment(3);
			$limit=5;
			if(!$page):
				$offset = 0;
			else:
				$offset = $page;
			endif;
			
			$data["kurir"] = $this->Adminmodel->lihat_tabel('toa_kurir','nama_kurir','ASC',$limit,$offset);
			
			$config['base_url'] = base_url() . 'webmaster/kurir/';
			$config['total_rows'] = $this->Adminmodel->hitung_isi_tabel('toa_kurir','');
			$config['per_page'] = $limit;
			$config['uri_segment'] = 3;
			$this->pagination->initialize($config);
			$data["paginator"] =$this->pagination->create_links();
			$data['tot'] = $offset;
			$this->load->view('webmaster/admin/header', $data);
			$this->load->view('webmaster/admin/kurir', $data);
			$this->load->view('webmaster/admin/footer', $data);
			
		}
		else
		{
			redirect('webmaster');
		}
			
	}

	public function tambah_kurir()
	{
		if($this->session->userdata('isLoggedIn') == "admin")
		{
			$this->load->view('webmaster/admin/header');
			$this->load->view('webmaster/admin/input_kurir');
			$this->load->view('webmaster/admin/footer');
		}
		else
		{
			redirect('webmaster');
		}
		
	}

	public function edit_kurir($kode="")
	{
		if($this->session->userdata('isLoggedIn') == "admin")
		{
			$id = isset($kode) ? $kode: $this->uri->segment(3);
			$kode_kurir = array('kode_kurir'=>$id);
			$q = $this->Adminmodel->ambilDataByID('toa_kurir',$kode_kurir);
			foreach ($q as $value) {
				$data['kode']  = $value->kode_kurir; 
				$data['nama_kurir']  = $value->nama_kurir; 
			}
			$data['st'] = 'edit';

			$this->load->view('webmaster/admin/header');
			$this->load->view('webmaster/admin/input_kurir',$data);
			$this->load->view('webmaster/admin/footer');
		}
		else
		{
			redirect('webmaster');
		}
	}

	public function validatekurir()
	{
		if($this->session->userdata('isLoggedIn') == "admin")
		{
			// set rules untuk proses validasi
			$this->form_validation->set_rules('nama_kurir', 'Nama kurir', 'trim|required');

			return $this->form_validation->run();			
		}
		else
		{
			redirect('webmaster');

		}
	}

	public function simpan_kurir()
	{
		if($this->session->userdata('isLoggedIn') == "admin")
		{
			$cek_kurir = $this->validatekurir();

			$st = $this->input->post('st', TRUE);
			$kode_kurir = ($st=="tambah") ? $this->Adminmodel->ambilKode('kode_kurir','toa_kurir','KR'):$this->input->post('kode', TRUE);

			if(!$cek_kurir)
			{
				return $hasil = ($st == "tambah") ? $this->tambah_kurir():$this->edit_kurir($kode=$kode_kurir);
			}


			$nama_kurir = $this->input->post('nama_kurir', TRUE);
			

			$data = array('kode_kurir'=>$kode_kurir, 'nama_kurir'=>$nama_kurir);

			if($st == 'tambah')
			{
				$this->Adminmodel->simpan_data('toa_kurir', $data);
				redirect('webmaster/kurir');
			}
			else if($st == 'edit')
			{
				$where = array('kode_kurir'=>$kode_kurir);
				$this->Adminmodel->update_data('toa_kurir', $data, $where);
				redirect('webmaster/kurir');
			}
			else 
			{
				redirect('webmaster');
			}
		}
		else
		{
			redirect('webmaster');
		}
	}

	public function hapus_kurir($id)
	{
		if($this->session->userdata('isLoggedIn') == "admin")
		{
			$cek = $this->Adminmodel->hapus_tabel_byId('toa_kurir','kode_kurir',$id);
			redirect('webmaster/kurir');
		}
		else
		{
			redirect('webmaster');
		}
		
	}
	// -----------------------------------------------------------------------------------------------------
	// Akhir fungsi kurir
	// -----------------------------------------------------------------------------------------------------


	// -----------------------------------------------------------------------------------------
	// Mulai fungsi testimoni
	// -----------------------------------------------------------------------------------------
	public function testimoni()
	{
		if($this->session->userdata('isLoggedIn') == "admin")
		{
			$page=$this->uri->segment(3);
			$limit=5;
			if(!$page):
				$offset = 0;
			else:
				$offset = $page;
			endif;
			
			$data["testimoni"] = $this->Adminmodel->lihat_tabel('toa_testimoni','status','ASC',$limit,$offset);
			
			$config['base_url'] = base_url() . 'webmaster/testimoni/';
			$config['total_rows'] = $this->Adminmodel->hitung_isi_tabel('toa_testimoni','');
			$config['per_page'] = $limit;
			$config['uri_segment'] = 3;
			$this->pagination->initialize($config);
			$data["paginator"] =$this->pagination->create_links();
			$data['tot'] = $offset;
			$this->load->view('webmaster/admin/header', $data);
			$this->load->view('webmaster/admin/testimoni', $data);
			$this->load->view('webmaster/admin/footer', $data);
			
		}
		else
		{
			redirect('webmaster');
		}
			
	}

	public function tambah_testimoni()
	{
		if($this->session->userdata('isLoggedIn') == "admin")
		{
			$this->load->view('webmaster/admin/header');
			$this->load->view('webmaster/admin/input_testimoni');
			$this->load->view('webmaster/admin/footer');
		}
		else
		{
			redirect('webmaster');
		}
		
	}

	public function aktifkan_testi($id)
	{
		if($this->session->userdata('isLoggedIn') == "admin")
		{
			$this->Adminmodel->update_data('toa_testimoni', array('status'=>1), array('kode_testimoni'=>$id));
			redirect('webmaster/testimoni');
		}
		else
		{
			redirect('webmaster');
		}
	}

	public function nonaktifkan_testi($id)
	{
		if($this->session->userdata('isLoggedIn') == "admin")
		{
			$this->Adminmodel->update_data('toa_testimoni', array('status'=>0), array('kode_testimoni'=>$id));
			redirect('webmaster/testimoni');
		}
		else
		{
			redirect('webmaster');
		}
	}

	public function edit_testimoni($kode="")
	{
		if($this->session->userdata('isLoggedIn') == "admin")
		{
			$id = isset($kode) ? $kode: $this->uri->segment(3);
			$kode_testimoni = array('kode_testimoni'=>$id);
			$q = $this->Adminmodel->ambilDataByID('toa_testimoni',$kode_testimoni);
			foreach ($q as $value) {
				$data['kode']  = $value->kode_testimoni; 
				$data['nama']  = $value->nama; 
				$data['email']  = $value->email; 
				$data['isi']  = $value->isi; 
				$data['status']  = $value->status; 
			}
			$data['st'] = 'edit';

			$this->load->view('webmaster/admin/header');
			$this->load->view('webmaster/admin/input_testimoni',$data);
			$this->load->view('webmaster/admin/footer');
		}
		else
		{
			redirect('webmaster');
		}
	}

	public function validatetestimoni()
	{
		if($this->session->userdata('isLoggedIn') == "admin")
		{
			// set rules untuk proses validasi
			$this->form_validation->set_rules('nama', 'Nama', 'trim|required');
			$this->form_validation->set_rules('email', 'Email', 'trim|required');
			$this->form_validation->set_rules('isi', 'Isi testimoni', 'trim|required');
			$this->form_validation->set_rules('status', 'Status', 'trim|required');

			return $this->form_validation->run();			
		}
		else
		{
			redirect('webmaster');

		}
	}

	public function simpan_testimoni()
	{
		if($this->session->userdata('isLoggedIn') == "admin")
		{
			$cek_testimoni = $this->validatetestimoni();

			$st = $this->input->post('st', TRUE);
			$kode_testimoni = ($st=="tambah") ? $this->Adminmodel->ambilKode('kode_testimoni','toa_testimoni','TEST'):$this->input->post('kode', TRUE);

			if(!$cek_testimoni)
			{
				return $hasil = ($st == "tambah") ? $this->tambah_testimoni():$this->edit_testimoni($kode=$kode_testimoni);
			}


			$nama = $this->input->post('nama', TRUE);
			$waktu = date('Y-m-d_H:i:s');
			$email = $this->input->post('email', TRUE);
			$isi = $this->input->post('isi', TRUE);
			$status = $this->input->post('status', TRUE);
			

			$data = array('kode_testimoni'=>$kode_testimoni, 'nama'=>$nama,'waktu'=>$waktu, 'email'=>$email, 'isi'=>$isi, 'status'=>$status);

			if($st == 'tambah')
			{
				$this->Adminmodel->simpan_data('toa_testimoni', $data);
				redirect('webmaster/testimoni');
			}
			else if($st == 'edit')
			{
				$where = array('kode_testimoni'=>$kode_testimoni);
				$this->Adminmodel->update_data('toa_testimoni', $data, $where);
				redirect('webmaster/testimoni');
			}
			else 
			{
				redirect('webmaster');
			}
		}
		else
		{
			redirect('webmaster');
		}
	}

	public function hapus_testimoni($id)
	{
		if($this->session->userdata('isLoggedIn') == "admin")
		{
			$cek = $this->Adminmodel->hapus_tabel_byId('toa_testimoni','kode_testimoni',$id);
			redirect('webmaster/testimoni');
		}
		else
		{
			redirect('webmaster');
		}
		
	}
	// -----------------------------------------------------------------------------------------------------
	// Akhir fungsi testimoni
	// -----------------------------------------------------------------------------------------------------

	// -----------------------------------------------------------------------------------------------------
	// Mulai fungsi pembayaran
	// -----------------------------------------------------------------------------------------------------
	public function pembayaran()
	{
		if($this->session->userdata('isLoggedIn') == "admin")
		{
			$page=$this->uri->segment(3);
			$limit=10;
			if(!$page):
				$offset = 0;
			else:
				$offset = $page;
			endif;
			$data["pembayaran"] = $this->Adminmodel->lihat_tabel('toa_pembayaran','tgl_bayar','DESC',$limit,$offset);
			$config['base_url'] = base_url() . 'webmaster/pembayaran/';
			$config['total_rows'] = $this->Adminmodel->hitung_isi_tabel('toa_pembayaran','');
			$config['per_page'] = $limit;
			$config['uri_segment'] = 3;
			$this->pagination->initialize($config);
			$data["paginator"] =$this->pagination->create_links();
			$data['tot'] = $offset;
			$this->load->view('webmaster/admin/header', $data);
			$this->load->view('webmaster/admin/pembayaran', $data);
			$this->load->view('webmaster/admin/footer', $data);
			
		}
		else
		{
			redirect('webmaster');
		}
			
	}

	public function pembayaranPending()
	{
		if($this->session->userdata('isLoggedIn') == "admin")
		{
			$page=$this->uri->segment(3);
			$limit=10;
			if(!$page):
				$offset = 0;
			else:
				$offset = $page;
			endif;
			$data["pembayaran"] = $this->db->get_where('toa_pembayaran',array('status'=>'PENDING'),$limit,$offset);
			$config['base_url'] = base_url() . 'webmaster/pembayaranPending/';
			$config['total_rows'] = $this->db->get_where('toa_pembayaran',array('status'=>'PENDING'))->num_rows();
			$config['per_page'] = $limit;
			$config['uri_segment'] = 3;
			$this->pagination->initialize($config);
			$data["paginator"] =$this->pagination->create_links();
			$data['tot'] = $offset;
			$this->load->view('webmaster/admin/header', $data);
			$this->load->view('webmaster/admin/pembayaran', $data);
			$this->load->view('webmaster/admin/footer', $data);
			
		}
		else
		{
			redirect('webmaster');
		}
			
	}

	public function pembayaranLunas()
	{
		if($this->session->userdata('isLoggedIn') == "admin")
		{
			$page=$this->uri->segment(3);
			$limit=10;
			if(!$page):
				$offset = 0;
			else:
				$offset = $page;
			endif;
			$data["pembayaran"] = $this->db->get_where('toa_pembayaran',array('status'=>'Lunas'),$limit,$offset);
			$config['base_url'] = base_url() . 'webmaster/pembayaranLunas/';
			$config['total_rows'] = $this->db->get_where('toa_pembayaran',array('status'=>'Lunas'))->num_rows();
			$config['per_page'] = $limit;
			$config['uri_segment'] = 3;
			$this->pagination->initialize($config);
			$data["paginator"] =$this->pagination->create_links();
			$data['tot'] = $offset;
			$this->load->view('webmaster/admin/header', $data);
			$this->load->view('webmaster/admin/pembayaran', $data);
			$this->load->view('webmaster/admin/footer', $data);
			
		}
		else
		{
			redirect('webmaster');
		}
			
	}

	public function konfirmasi_pembayaran_valid($kode_bayar, $kode_order)
	{
		if($this->session->userdata('isLoggedIn') == "admin")
		{
			$this->Adminmodel->update_data('toa_order', array('status'=>'Lunas'), array('kode_order' =>$kode_order));
			$this->Adminmodel->update_data('toa_pembayaran', array('status'=>'Lunas'), array('kode_pembayaran'=>$kode_bayar, 'kode_order'=>$kode_order));
			$order = $this->db->get_where('toa_order', array('kode_order'=>$kode_order));
			foreach ($order->result() as $value) {
			 	$data['kode_member'] = $value->kode_member;
			 	$data['alamat1'] = $value->alamat1;
			 	$data['alamat2'] = $value->alamat2;
			 	$data['sub_total'] = $value->sub_total;
			 	$data['sub_total'] = $value->sub_total;
			 	$data['ongkir'] = $value->ongkir;
			 	$data['total_bayar'] = $value->total_bayar;
			 } 

			 $mail = $this->db->get_where('toa_member', array('kode_member'=>$data['kode_member']));
			 foreach ($mail->result() as $value2) {
			 	$email = $value2->email;
			 }
			$message = 
			"			
			<table style='width:100%;border-collapse:collapse;border:3px solid #F9F9F9;box-shadow:0 0 7px #FE980F;background:#FE980F;padding:10px;font-size:18px;font-family:simplex;color:#FFF;'>
			<thead style='background:#FCF8E3;color:#FE980F;border-radius:5px;padding:10px;margin:10px;'>
			<tr>
			<th colspan='2'><h3>Pembayaran Anda Sudah Kami Terima</h3></th>
			</tr>
			<tr>
			<th colspan='2'>Kami sedang memproses pengiriman barang pesanan anda. Silakan tunggu email pemberitahuan selanjutnya yang berisi detail pengiriman beserta nomor resi pengiriman.</th>
			</tr>
			<tr>
			<th colspan='2'>Detail Pengiriman dan Pembayaran.</th>
			</tr>
			</thead>
			<tbody style='background:#FCF8E3;color:#FE980F;border-radius:5px;padding:10px;margin:10px;'>
			<tr>
			<td colspan='2'>" .$data['alamat1']."<br>".$data['alamat2']. "</td>
			</tr>
			<tr>
			<td>Sub Total</td>
			<td>: Rp ".number_format($data['sub_total'], 2)."</td>
			</tr>
			<tr>
			<td>Ongkir</td>
			<td>: Rp ".number_format($data['ongkir'], 2)."</td>
			</tr>
			<tr>
			<td><b>Total Pembayaran</b></td>
			<td><b>: Rp ".number_format($data['total_bayar'], 2)."</b></td>
			</tr>
			<tr>
			<td colspan='2'>.</td>
			</tr>
			</tbody>
			</table>
			";
			$this->load->library('email');
			$this->email->set_newline("\r\n");
			$this->email->from('ehaku@momandkidsgoods.com','Admin MomAndKidsGoods[dot]com');
			$this->email->to($email); 
			$this->email->subject('Pembayaran Sudah Diterima');
			$this->email->message($message);
			if($this->email->send())
			{
				$this->session->set_flashdata('result', 'Email Konfirmasi Pembayaran Valid berhasil dikirim..');
				redirect('webmaster/pembayaran');
			}
			else
			{
				$this->session->set_flashdata('result', 'Email Konfirmasi Pembayaran Valid Gagal dikirim..');
				redirect('webmaster/pembayaran');
			}

			}
			else
			{
				redirect('webmaster');
			}
	}

	public function konfirmasi_pembayaran_invalid($kode_bayar, $kode_order)
	{
		if($this->session->userdata('isLoggedIn') == "admin")
		{
			$this->Adminmodel->update_data('toa_order', array('status'=>'Belum Bayar'), array('kode_order' =>$kode_order));
			$this->db->delete('toa_pembayaran', array('kode_pembayaran'=>$kode_bayar, 'kode_order'=>$kode_order));
			$order = $this->db->get_where('toa_order', array('kode_order'=>$kode_order));
			foreach ($order->result() as $value) {
			 	$data['kode_member'] = $value->kode_member;
			 	$data['alamat1'] = $value->alamat1;
			 	$data['alamat2'] = $value->alamat2;
			 	$data['sub_total'] = $value->sub_total;
			 	$data['sub_total'] = $value->sub_total;
			 	$data['ongkir'] = $value->ongkir;
			 	$data['total_bayar'] = $value->total_bayar;
			 } 

			 $mail = $this->db->get_where('toa_member', array('kode_member'=>$data['kode_member']));
			 foreach ($mail->result() as $value2) {
			 	$email = $value2->email;
			 }
			$message = 
			"
			<table style='width:100%;border-collapse:collapse;border:3px solid #F9F9F9;box-shadow:0 0 7px #FE980F;background:#FE980F;padding:10px;font-size:18px;font-family:simplex;color:#FFF;'>
			<thead style='background:#FCF8E3;color:#FE980F;border-radius:5px;padding:10px;margin:10px;'>
			<tr>
			<th colspan='2'><h3>Konfirmasi Pembayaran Tidak Valid</h3></th>
			</tr>
			<tr>
			<th colspan='2'>Konfirmasi Pembayaran yang anda lakukan tidak sesuai dengan data pemesanan anda.</th>
			</tr>
			<tr>
			<th colspan='2'>Silakan lakukan pembayaran sesuai dengan data pesanan anda dan konfirmasi ulang pembayaran anda.</th>
			</tr>
			<tr>
			<th colspan='2'>Jika anda merasa bahwa pembayaran yang anda lakukan sudah benar, silakan hubungi costumer service kami untuk info lebih lanjut.</th>
			</tr>
			<tr>
			<th colspan='2'>Detail Pengiriman dan Pembayaran.</th>
			</tr>
			</thead>
			<tbody style='background:#FCF8E3;color:#FE980F;border-radius:5px;padding:10px;margin:10px;'>
			<tr>
			<td colspan='2'>" .$data['alamat1']."<br>".$data['alamat2']. "</td>
			</tr>
			<tr>
			<td>Sub Total</td>
			<td>: Rp ".number_format($data['sub_total'], 2)."</td>
			</tr>
			<tr>
			<td>Ongkir</td>
			<td>: Rp ".number_format($data['ongkir'], 2)."</td>
			</tr>
			<tr>
			<td><b>Total Pembayaran</b></td>
			<td><b>: Rp ".number_format($data['total_bayar'], 2)."</b></td>
			</tr>
			<tr>
			<td colspan='2'>.</td>
			</tr>
			</tbody>
			</table>
			";
			$this->load->library('email');
			$this->email->set_newline("\r\n");
			$this->email->from('ehaku@momandkidsgoods.com','Admin MomAndKidsGoods[dot]com');
			$this->email->to($email); 
			$this->email->subject('Data Pembayaran Tidak Valid');
			$this->email->message($message);
			if($this->email->send())
			{
				$this->session->set_flashdata('result', 'Email Konfirmasi Pembayaran Tidak Valid berhasil dikirim..');
				redirect('webmaster/pembayaran');
			}
			else
			{
				$this->session->set_flashdata('result', 'Email Konfirmasi Pembayaran Tidak Valid Gagal dikirim..');
				redirect('webmaster/pembayaran');
			}

			}
			else
			{
				redirect('webmaster');
			}
	}

	public function cari_pembayaran()
	{

		if($this->session->userdata('isLoggedIn') == "admin")
		{
			if($this->input->post("cari")=="")
			{
				$kata = $this->session->userdata('kata');
			}
			else
			{
				$sess_data['kata'] = $this->input->post("cari");
				$this->session->set_userdata($sess_data);
				$kata = $this->session->userdata('kata');
			}
			
			$page=$this->uri->segment(3);
			$limit=5;
			if(!$page):
			$offset = 0;
			else:
			$offset = $page;
			endif;
			
			$d['tot'] = $offset;
			$config['base_url'] = base_url() . 'webmaster/cari_pembayaran/';
			$config['total_rows'] = $this->Adminmodel->hitung_isi_tabel_cari('toa_pembayaran', 'tgl_bayar', $kata);
			$config['per_page'] = $limit;
			$config['uri_segment'] = 3;
			$this->pagination->initialize($config);
			$d["paginator"] =$this->pagination->create_links();
			
			$where = array('tgl_bayar'=>$kata);
			$d['pembayaran'] = $this->Adminmodel->lihat_tabel_cari('toa_pembayaran','tgl_bayar', 'ASC', $limit, $offset, $where);
			$this->load->view('webmaster/admin/header');
			$this->load->view('webmaster/admin/pembayaran', $d);
			$this->load->view('webmaster/admin/footer');

			
		}
		else
		{
			redirect('webmaster');
		}
			
	}


	public function hapus_pembayaran($id)
	{
		if($this->session->userdata('isLoggedIn') == "admin")
		{
			$cek = $this->Adminmodel->hapus_tabel_byId('toa_pembayaran','kode_pembayaran',$id);
			redirect('webmaster/pembayaran');
		}
		else
		{
			redirect('webmaster');
		}
		
	}
	// -----------------------------------------------------------------------------------------------------
	// Akhir fungsi pembayaran
	// -----------------------------------------------------------------------------------------------------

	// -----------------------------------------------------------------------------------------------------
	// mulai fungsi backup
	// -----------------------------------------------------------------------------------------------------
	public function backup()
	{
		if($this->session->userdata('isLoggedIn') == "admin")
		{
			// Load the DB utility class
			$this->load->dbutil();

			// Backup your entire database and assign it to a variable
			$backup =& $this->dbutil->backup();
			$name = 'backup-'.date('Y-m-d-H-i-s').'.gz';
			// Load the file helper and write the file to your server
			$this->load->helper('file');
			write_file('/path/to/'.$name, $backup);

			// Load the download helper and send the file to your desktop
			$this->load->helper('download');
			force_download($name, $backup);
		}
		else 
		{
			redirect('webmaster');
		}
	}


}
/* End of file Web.php */
/* Location: ./application/controllers/Web.php */