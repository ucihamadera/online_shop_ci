<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Web extends CI_Controller {


	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$data = $this->db->get('toa_info');

		$d['banner'] = $this->db->get('toa_banner');

		$d['produk'] = $this->Adminmodel->lihat_tabel('toa_produk', 'tgl_insert', 'DESC', 6, 0);
		$d['kategori'] = $this->db->get_where('toa_kategori', array('kode_level'=>0, 'kode_parent'=>0));
		$d['art_active'] = $this->Adminmodel->lihat_tabel('toa_artikel', 'tanggal', 'DESC', 3,0);
		$d['art_item'] = $this->Adminmodel->lihat_tabel('toa_artikel', 'tanggal', 'DESC', 3,3);
		$d['judul'] = 'Beranda | ';
		$this->load->view('web/header',$d);
		$this->load->view('web/banner',$d);
		$this->load->view('web/sidebar',$d);
		$this->load->view('web/main',$d);
		$this->load->view('web/footer',$d);
	}

	
	

}

/* End of file  */
/* Location: ./application/controllers/ */