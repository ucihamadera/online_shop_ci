<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Artikel extends CI_Controller {

	public function index()
	{
		redirect('artikel/cari');
	}

	public function cari()
	{
		$d['judul'] = 'Semua Artikel | ';
		$page=$this->uri->segment(3);
		$limit=2;
		if(!$page):
			$offset = 0;
		else:
			$offset = $page;
		endif;
		
		$d["artikel"] = $this->Adminmodel->lihat_tabel('toa_artikel','tanggal','DESC',$limit,$offset);
		
		$config['base_url'] = base_url() . 'artikel/cari';
		$config['total_rows'] = $this->Adminmodel->hitung_isi_tabel('toa_artikel','');
		$config['per_page'] = $limit;
		$config['uri_segment'] = 3;
		$this->pagination->initialize($config);
		$d["paginator"] =$this->pagination->create_links();
		$d['tot'] = $offset; 
		$d['kategori'] = $this->db->get_where('toa_kategori', array('kode_level'=>0, 'kode_parent'=>0));


		$this->load->view('web/header',$d);
		$this->load->view('web/sidebar',$d);
		$this->load->view('web/artikel',$d);
		$this->load->view('web/footer',$d);
	}

	public function detail($link)
	{

		$kode = $this->uri->segment(3) ? $this->uri->segment(3):'';		
	
		$p_kode = explode("/",$kode);
		$d['detail_artikel'] = $this->db->get_where('toa_artikel', array('kode_artikel'=>$p_kode[0]));
		$this->Webmodel->HitungBaca($p_kode[0]);
		foreach($d['detail_artikel']->result() as $dp)
		{
			$d['kode'] = $dp->kode_artikel;
			$d['tanggal'] = $dp->tanggal;
			$d['jam'] = $dp->jam;
			$d['isi_artikel'] = $dp->isi_artikel;
			$d['gambar'] = $dp->gambar;
			$d['judul'] = $dp->judul;
			$k['judul'] = $dp->judul.' | ';
			$temp = explode('.', $dp->isi_artikel);
			$k['deskripsi'] = strip_tags($temp[0]);
		}
		
		// $d['nama'] = 'Mom And Kids Goods | Detail artikel';
		// $d['deskripsi'] = 'Semua artikel Terbaru dari Toko Mom And Kids Goods';
		// $d['keywords'] = 'toko_online, toko_peralatan_bayi, toko peralatan_rumah_tangga,';
		$data = $this->db->get('toa_info');
		$d['kategori'] = $this->db->get_where('toa_kategori', array('kode_level'=>0, 'kode_parent'=>0));

		// foreach ($data->result() as $value) {
		// 	$d['alamat'] =$value->alamat;
		// 	$d['no_telp'] =$value->no_telp;
		// 	$d['email'] =$value->email;
		// 	$d['google'] =$value->google;
		// 	$d['fb'] =$value->fb;
		// 	$d['twitter'] =$value->twitter;
		// 	$d['logo'] =$value->logo;
		// }
		$d['art_item'] = $this->Adminmodel->lihat_tabel('toa_artikel', 'tanggal', 'DESC', 3,0);
		$d['art_active'] = $this->Adminmodel->lihat_tabel('toa_artikel', 'tanggal', 'DESC', 3,3);


		$this->load->view('web/header',$k);
		$this->load->view('web/sidebar',$d);
		$this->load->view('web/detail_artikel',$d);
		$this->load->view('web/footer',$d);
		
	
	}

}

/* End of file Artikel.php */
/* Location: ./application/controllers/Artikel.php */